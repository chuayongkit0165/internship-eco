<?php
/*
 * @package    VirtueMart
 * @subpackage Plugins  - Elements
 * @package VirtueMart
 * @subpackage
 * @author DM
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
defined('_JEXEC') or die('Direct Access to ' . basename(__FILE__) . 'is not allowed.');

if (!defined('_VALID_MOS') && !defined('_JEXEC'))
    die('Direct Access to ' . basename(__FILE__) . ' is not allowed.');

if (!class_exists('vmPSPlugin'))
    require(JPATH_VM_PLUGINS . DS . 'vmpsplugin.php');

class plgVmPaymentFondy extends vmPSPlugin
{
    /* instance of class  */
    public static $_this = false;
    
    function __construct(&$subject, $config)
    {

        parent::__construct($subject, $config);
        $jlang = JFactory::getLanguage();
        $jlang->load('plg_vmpayment_fondy', JPATH_ADMINISTRATOR, NULL, TRUE);
        $this->_loggable   = true;
        $this->tableFields = array_keys($this->getTableSQLFields());
        $this->_tablepkey = 'id';
        $this->_tableId = 'id';
        $varsToPush        = array(
          'payment_logos' => array('','char'),
          'countries' => array(0,'int'),
          'payment_currency' => array(0,'int'),
          'merchant_id' => array('','string'),
          'secret_key' => array('','string'),
		  'currency' => array('','string'),
          'status_success' => array('','char'),
          'status_canceled' => array('','char')
        );
        $this->setConfigParameterable($this->_configTableFieldName, $varsToPush);
    }
    
      
    protected function getVmPluginCreateTableSQL()
    {
        return $this->createTableSQL('Payment Fondy Table');
    }
    
    function getTableSQLFields()
    {
       
        $SQLfields = array('id' => 'tinyint(1) unsigned NOT NULL AUTO_INCREMENT',
                           'virtuemart_order_id' => 'int(11) UNSIGNED DEFAULT NULL',
                           'order_number' => 'char(32) DEFAULT NULL',
                           'virtuemart_paymentmethod_id' => 'mediumint(1) UNSIGNED DEFAULT NULL',
                           'payment_name' => 'char(255) NOT NULL DEFAULT \'\' ',
                           'payment_order_total' => 'decimal(15,5) NOT NULL DEFAULT \'0.00000\' ',
                           'payment_currency' => 'char(3) ',
                           'cost_per_transaction' => ' decimal(10,2) DEFAULT NULL ',
                           'cost_percent_total' => ' decimal(10,2) DEFAULT NULL ',
                           'tax_id' => 'smallint(11) DEFAULT NULL');

        return $SQLfields;
    }
    
    function plgVmConfirmedOrder($cart, $order)
    {
        if (!($method = $this->getVmPluginMethod($order['details']['BT']->virtuemart_paymentmethod_id))) {
            return null; // Another method was selected, do nothing
        }
        if (!$this->selectedThisElement($method->payment_element)) {
            return false;
        }
        
		include_once(dirname(__FILE__) . DS . "/fondy/Fondy.cls.php");
        if (!class_exists('VirtueMartModelCurrency')) {
            require(JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'currency.php');
        }

        JFactory::getLanguage()->load($filename = 'com_virtuemart', JPATH_ADMINISTRATOR);
        $vendorId = 0;

        $html = "";

        if (!class_exists('VirtueMartModelOrders')) {
            require(JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php');
        }

        $this->getPaymentCurrency($method);

        $currency = $method->currency;
    

        list($lang,) = explode('-', JFactory::getLanguage()->getTag());

        $paymentMethodID = $order['details']['BT']->virtuemart_paymentmethod_id;
        $responseUrl = JROUTE::_(JURI::root().'index.php?option=com_virtuemart&view=orders&layout=details&order_number=' . $order['details']['BT']->order_number . '&order_pass=' . $order['details']['BT']->order_pass);
        $callbackUrl = JROUTE::_(JURI::root() . 'index.php?option=com_virtuemart&view=pluginresponse&task=pluginnotification&tmpl=component&pm=' . $paymentMethodID);
		
        $user = & $cart->BT;

        $formFields = array('order_id' => $order['details']['BT']->order_number . Fondy::ORDER_SEPARATOR . time(),
                             'merchant_id' => $method->merchant_id,
                             'order_desc' =>   $desc = 'Order Pay №' . $order['details']['BT']->order_number,
                             'amount' => Fondy::getAmount($order),
                             'currency' => $currency,
                             'server_callback_url' => $callbackUrl,
							 'response_url' => $responseUrl,						  
                             'lang' => strtoupper($lang),
                             'sender_email' => $user['email']);

        $formFields['signature'] = Fondy::getSignature($formFields, $method->secret_key);

		
        $fondyArgsArray = array();
        foreach ($formFields as $key => $value) {
            $fondyArgsArray[] = "<input type='hidden' name='$key' value='$value'/>";
        }

        $html = '	<form action="' . Fondy::URL . '" method="post" id="fondy_payment_form">
  				' . implode('', $fondyArgsArray) .
            '</form>' .
            "<div><img src='https://fondy.eu/img/loader.gif' width='50px' style='margin:20px 20px;'></div>".
            "<script> setTimeout(function() {
                 document.getElementById('fondy_payment_form').submit();
             }, 100);
            </script>";
			
		
       return $this->processConfirmedOrderPaymentResponse(true, $cart, $order, $html, $this->renderPluginName($method, $order), 'P');
    }
    
    function plgVmOnShowOrderBEPayment($virtuemart_order_id, $virtuemart_payment_id)
    {
        if (!$this->selectedThisByMethodId($virtuemart_payment_id)) {
            return null; // Another method was selected, do nothing
        }
        
        $db = JFactory::getDBO();
        $q  = 'SELECT * FROM `' . $this->_tablename . '` ' . 'WHERE `virtuemart_order_id` = ' . $virtuemart_order_id;
        $db->setQuery($q);
        if (!($paymentTable = $db->loadObject())) {
            vmWarn(500, $q . " " . $db->getErrorMsg());
            return '';
        }
        $this->getPaymentCurrency($paymentTable);
        
        $html = '<table class="adminlist">' . "\n";
        $html .= $this->getHtmlHeaderBE();
        $html .= $this->getHtmlRowBE('STANDARD_PAYMENT_NAME', $paymentTable->payment_name);
        $html .= $this->getHtmlRowBE('STANDARD_PAYMENT_TOTAL_CURRENCY', $paymentTable->payment_order_total . ' ' . $paymentTable->payment_currency);
        $html .= '</table>' . "\n";
        return $html;
    }
    
    function getCosts(VirtueMartCart $cart, $method, $cart_prices)
    {
        return 0;
    }
    
    protected function checkConditions($cart, $method, $cart_prices)
    {
        return true;
    }
    
    function plgVmOnStoreInstallPaymentPluginTable($jplugin_id)
    {
        return $this->onStoreInstallPluginTable($jplugin_id);
    }
    
    public function plgVmOnSelectCheckPayment(VirtueMartCart $cart)
    {
        return $this->OnSelectCheck($cart);
    }
    
    public function plgVmDisplayListFEPayment(VirtueMartCart $cart, $selected = 0, &$htmlIn)
    {
        return $this->displayListFE($cart, $selected, $htmlIn);
    }
    
    public function plgVmonSelectedCalculatePricePayment(VirtueMartCart $cart, array &$cart_prices, &$cart_prices_name)
    {
        return $this->onSelectedCalculatePrice($cart, $cart_prices, $cart_prices_name);
    }
    
    function plgVmgetPaymentCurrency($virtuemart_paymentmethod_id, &$paymentCurrencyId)
    {
        if (!($method = $this->getVmPluginMethod($virtuemart_paymentmethod_id))) {
            return null; // Another method was selected, do nothing
        }
        if (!$this->selectedThisElement($method->payment_element)) {
            return false;
        }
        $this->getPaymentCurrency($method);
        
        $paymentCurrencyId = $method->payment_currency;
    }
    
    function plgVmOnCheckAutomaticSelectedPayment(VirtueMartCart $cart, array $cart_prices = array())
    {
        return $this->onCheckAutomaticSelected($cart, $cart_prices);
    }
    
    public function plgVmOnShowOrderFEPayment($virtuemart_order_id, $virtuemart_paymentmethod_id, &$payment_name)
    {
        $this->onShowOrderFE($virtuemart_order_id, $virtuemart_paymentmethod_id, $payment_name);
    }
    
    function plgVmonShowOrderPrintPayment($order_number, $method_id)
    {
        return $this->onShowOrderPrint($order_number, $method_id);
    }
    
    function plgVmDeclarePluginParamsPayment($name, $id, &$data)
    {
        return $this->declarePluginParams('payment', $name, $id, $data);
    }
    
    function plgVmSetOnTablePluginParamsPayment($name, $id, &$table)
    {
        return $this->setOnTablePluginParams($name, $id, $table);
    }
    
    protected function displayLogos($logo_list)
    {
        $img = "";
        
        if (!(empty($logo_list))) {
            $url = JURI::root() . 'plugins/vmpayment/fondy/';
            if (!is_array($logo_list))
                $logo_list = (array) $logo_list;
            foreach ($logo_list as $logo) {
                $alt_text = substr($logo, 0, strpos($logo, '.'));
                $img .= '<img align="middle" src="' . $url . $logo . '"  alt="' . $alt_text . '" /> ';
            }
        }
        return $img;
    }
    
    public function plgVmOnPaymentNotification()
    {
		$callback = JRequest::get( 'post' );
       	If (empty($callback)){
            $fap = json_decode(file_get_contents("php://input"));
            foreach($fap as $key=>$val)
            {
                $callback[$key] =  $val ;
            }
		}
        if (!class_exists('VirtueMartModelOrders'))
            require(JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php');
        if (!class_exists('plgVmPaymentFondy'))
            require(dirname(__FILE__). DS . 'fondy.php');
        require(dirname(__FILE__). DS . '/fondy/Fondy.cls.php');
        list($order_id,) = explode(Fondy::ORDER_SEPARATOR, $callback['order_id']);
        $order = new VirtueMartModelOrders();
        $order_s_id = $order->getOrderIdByOrderNumber($order_id);

        $db = JFactory::getDBO();
        $query = "SELECT * FROM #__virtuemart_orders WHERE virtuemart_order_id =".$order_s_id;
        $db->setQuery($query);
        $payment = $db->loadObject();
        $method = $this->getVmPluginMethod($payment->virtuemart_paymentmethod_id);
        $orderitems = $order->getOrder($order_s_id);
        $option  = array(   'merchant_id' => $method->merchant_id,
            'secret_key' =>  $method->secret_key);
		
        $response = Fondy::isPaymentValid($option, $callback);

        if ($response == true && $callback['order_status'] == 'approved') {
            $orderitems['order_status'] = $method->status_success;
			
            $orderitems['customer_notified'] = 1;
            $orderitems['virtuemart_order_id'] = $order_s_id;
            $orderitems['comments'] = 'Fondy ID: '.$order_id. " Ref ID : ". $callback['payment_id'];
            $order->updateStatusForOneOrder($order_s_id, $orderitems, true);
            $red = JROUTE::_(JURI::root().'index.php?option=com_virtuemart&view=orders&layout=details&order_number=' . $orderitems['details']['BT']->order_number . '&order_pass=' . $orderitems['details']['BT']->order_pass);
            $allDone =& JFactory::getApplication();

            $datetime = date("YmdHis");
            echo "OK";
        } else {
            $orderitems['order_status'] = $method->status_canceled;
            $order->updateStatusForOneOrder($order_s_id, $orderitems, true);
            echo "<!-- {$response} -->";
        }


    }
    
    
    function plgVmOnPaymentResponseReceived(&$html)
    {
        // the payment itself should send the parameter needed;
         $method = $this->getVmPluginMethod(JRequest::getInt('pm', 0));
        if (!$this->selectedThisElement($method->payment_element)) {
            return false;
        }

        if (!class_exists('VirtueMartCart'))
            require(JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php');

        // get the correct cart / session
        $cart = VirtueMartCart::getCart();
        $cart->emptyCart();

        return true;
    }
    
    function plgVmOnUserPaymentCancel()
    {
       $data = JRequest::get('get');

        list($order_id,) = explode(Fondy::ORDER_SEPARATOR, $data['order_id']);
        $order = new VirtueMartModelOrders();

        $order_s_id = $order->getOrderIdByOrderNumber($order_id);
        $orderitems = $order->getOrder($order_s_id);

        $method = $this->getVmPluginMethod($orderitems['details']['BT']->virtuemart_paymentmethod_id);
        if (!$this->selectedThisElement($method->payment_element)) {
            return false;
        }

        if (!class_exists('VirtueMartModelOrders'))
            require(JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php');

        $this->handlePaymentUserCancel($data['oid']);

        return true;
    }

    private function notifyCustomer($order, $order_info)
    {
        $lang     = JFactory::getLanguage();
        $filename = 'com_virtuemart';
        $lang->load($filename, JPATH_ADMINISTRATOR);
        if (!class_exists('VirtueMartControllerVirtuemart'))
            require(JPATH_VM_SITE . DS . 'controllers' . DS . 'virtuemart.php');
        
        if (!class_exists('shopFunctionsF'))
            require(JPATH_VM_SITE . DS . 'helpers' . DS . 'shopfunctionsf.php');
        $controller = new VirtueMartControllerVirtuemart();
        $controller->addViewPath(JPATH_VM_ADMINISTRATOR . DS . 'views');
        
        $view = $controller->getView('orders', 'html');
        if (!$controllerName)
            $controllerName = 'orders';
        $controllerClassName = 'VirtueMartController' . ucfirst($controllerName);
        if (!class_exists($controllerClassName))
            require(JPATH_VM_SITE . DS . 'controllers' . DS . $controllerName . '.php');
        
        $view->addTemplatePath(JPATH_COMPONENT_ADMINISTRATOR . '/views/orders/tmpl');
        
        $db = JFactory::getDBO();
        $q  = "SELECT CONCAT_WS(' ',first_name, middle_name , last_name) AS full_name, email, order_status_name
			FROM #__virtuemart_order_userinfos
			LEFT JOIN #__virtuemart_orders
			ON #__virtuemart_orders.virtuemart_user_id = #__virtuemart_order_userinfos.virtuemart_user_id
			LEFT JOIN #__virtuemart_orderstates
			ON #__virtuemart_orderstates.order_status_code = #__virtuemart_orders.order_status
			WHERE #__virtuemart_orders.virtuemart_order_id = '" . $order['virtuemart_order_id'] . "'
			AND #__virtuemart_orders.virtuemart_order_id = #__virtuemart_order_userinfos.virtuemart_order_id";
        $db->setQuery($q);
        $db->query();
        $view->user  = $db->loadObject();
        $view->order = $order;
        JRequest::setVar('view', 'orders');
        $user = $this->sendVmMail($view, $order_info['details']['BT']->email, false);
        if (isset($view->doVendor)) {
            $this->sendVmMail($view, $view->vendorEmail, true);
        }
    }

    private function sendVmMail(&$view, $recipient, $vendor = false)
    {
        ob_start();
        $view->renderMailLayout($vendor, $recipient);
        $body = ob_get_contents();
        ob_end_clean();
        
        $subject = (isset($view->subject)) ? $view->subject : JText::_('COM_VIRTUEMART_DEFAULT_MESSAGE_SUBJECT');
        $mailer  = JFactory::getMailer();
        $mailer->addRecipient($recipient);
        $mailer->setSubject($subject);
        $mailer->isHTML(VmConfig::get('order_mail_html', true));
        $mailer->setBody($body);
        
        if (!$vendor) {
            $replyto[0] = $view->vendorEmail;
            $replyto[1] = $view->vendor->vendor_name;
            $mailer->addReplyTo($replyto);
        }
        
        if (isset($view->mediaToSend)) {
            foreach ((array) $view->mediaToSend as $media) {
                $mailer->addAttachment($media);
            }
        }
        return $mailer->Send();
    }
  function plgVmDeclarePluginParamsPaymentVM3( &$data) {
    return $this->declarePluginParams('payment', $data);
  }
}
