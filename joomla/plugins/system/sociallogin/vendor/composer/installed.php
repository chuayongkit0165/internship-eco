<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-main',
    'version' => 'dev-main',
    'aliases' => 
    array (
    ),
    'reference' => 'f24979e18031a6f00e2a834bf9eac5c5a7631eec',
    'name' => 'akeeba/sociallogin',
  ),
  'versions' => 
  array (
    'akeeba/sociallogin' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
      ),
      'reference' => 'f24979e18031a6f00e2a834bf9eac5c5a7631eec',
    ),
    'codercat/jwk-to-pem' => 
    array (
      'pretty_version' => '1.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e428b7abba5b37676e30e968930f718cf26724ac',
    ),
    'lcobucci/jwt' => 
    array (
      'pretty_version' => '3.4.2',
      'version' => '3.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '17cb82dd625ccb17c74bf8f38563d3b260306483',
    ),
    'phpseclib/bcmath_compat' => 
    array (
      'pretty_version' => '1.0.5',
      'version' => '1.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '89cbb63742a32730b7187773a60b6b12b9db4479',
    ),
    'phpseclib/phpseclib' => 
    array (
      'pretty_version' => '2.0.29',
      'version' => '2.0.29.0',
      'aliases' => 
      array (
      ),
      'reference' => '497856a8d997f640b4a516062f84228a772a48a8',
    ),
  ),
);
