<?php

/**
 * @package     Joomla
 * @subpackage  CoalaWeb Contact
 * @author      Steven Palmer <support@coalaweb.com>
 * @link        https://coalaweb.com/
 * @license     GNU/GPL V3 or later; https://www.gnu.org/licenses/gpl-3.0.html
 * @copyright   Copyright (c) 2020 Steven Palmer All rights reserved.
 *
 * CoalaWeb Contact is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

defined('_JEXEC') or die('Restricted access');

JLoader::import('joomla.plugin.plugin');
JLoader::import('joomla.application.plugin');
JLoader::import('joomla.filesystem.path');
JLoader::import('joomla.filesystem.file');
JLoader::import('joomla.utilities.version');
JLoader::import('joomla.log.log');

$path = '/components/com_coalawebcontact/helpers/';
JLoader::register('CoalawebcontactHelper', JPATH_ADMINISTRATOR . $path . 'coalawebcontact.php');

// Load version.php
$version_php = JPATH_ADMINISTRATOR . '/components/com_coalawebcontact/version.php';
if (!defined('COM_CWCONTACT_VERSION') && JFile::exists($version_php)) {
    include_once $version_php;
}

/**
 * Class plgSystemCwmailcheck
 */
class plgSystemCwmailcheck extends JPlugin {

    private $comParams;
    private $checkOk;
    private $debug;

    /**
     * plgSystemCwmailcheck constructor.
     * @param $subject
     * @param $config
     */
    public function __construct(& $subject, $config)
    {
        parent::__construct($subject, $config);

        // Load the language files
        $jlang = JFactory::getLanguage();

        // Plugin
        $jlang->load('plg_system_cwmailcheck', JPATH_ADMINISTRATOR, 'en-GB', true);
        $jlang->load('plg_system_cwmailcheck', JPATH_ADMINISTRATOR, $jlang->getDefault(), true);
        $jlang->load('plg_system_cwmailcheck', JPATH_ADMINISTRATOR, null, true);

        // Component
        $jlang->load('com_coalawebcontact', JPATH_ADMINISTRATOR, 'en-GB', true);
        $jlang->load('com_coalawebcontact', JPATH_ADMINISTRATOR, $jlang->getDefault(), true);
        $jlang->load('com_coalawebcontact', JPATH_ADMINISTRATOR, null, true);

        // Check if dependencies
        $this->checkOk = $this->checkDependencies();

        //Lets get our parameters together
        $this->comParams = $this->getComponentParameters('com_coalawebcontact');
        $this->debug = null !== $this->params->get('debug') ? $this->params->get('debug') : $this->comParams->get('debug', '0');
    }

    /**
     * OnAfterRender trigger
     *
     * @return bool|void
     */
    function onAfterRender() {
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();
        $option = $app->input->get('option');

        // Lets do a few checks first
        if (
            $app->getName() == 'site' ||
            $doc->getType() !== 'html' ||
            $option != 'com_config') {
            return;
        }

        // Dependency checks
        if ($this->checkOk['ok'] === false) {
            if ($this->debug === '1') {
                JFactory::getApplication()->enqueueMessage($this->checkOk['msg'], $this->checkOk['type']);
            }
            return;
        }

        //Do we want a button?
        $loadBtn = $this->comParams->get('check_btn_on', '1');
        if (!$loadBtn) {
            return;
        }

        //Let now load jquery
        JHtml::_('jquery.framework');
        //Now grab our script to append to the end of the page
        $html = $this->_mailcheckScripts();

        //Retrieve the body and then send it back with our script attached
        $bodyBefore = $app->getBody();
        $bodyAfter = str_replace('</body>', $html . '</body>', $bodyBefore);
        $app->setBody($bodyAfter);

        return true;
    }

    /**
     * OnAfterRoute trigger
     *
     * @return bool|void
     */
    function onAfterRoute() {
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();
        $input = JFactory::getApplication()->input;
        $option = $app->input->get('option');
        
        // Lets do a few checks first
        if (
            $app->getName() == 'site' ||
            $doc->getType() !== 'html' ||
            $option != 'com_config') {
            return;
        }

        // Dependency checks
        if ($this->checkOk['ok'] === false) {
            if ($this->debug === '1') {
                JFactory::getApplication()->enqueueMessage($this->checkOk['msg'], $this->checkOk['type']);
            }
            return;
        }

        //Do we want a button?
        $loadBtn = $this->comParams->get('check_btn_on', '1');
        if (!$loadBtn) {
            return;
        }
        
        //Lets add our custom alert files
        $baseUrl = '/media/coalawebcontact/plugins/system/cwmailcheck/';
        $doc->addStyleSheet($baseUrl . "css/sweetalert.css");
        $doc->addScript($baseUrl . "js/sweetalert.min.js");

        //now check our count to keep track of what stage we are at.
        $checkcount = $input->get('plg_cwmailcheck', false);

        if ($option != 'com_config' || !$checkcount) {
            return true;
        }

        //initiate the mailer 
        $mailer = self::createMailer();

        //Now send the mail and do some basic validation
        if ($mailer->Send() !== true) {
            $this->_error();
        }

        $this->_success();
    }

    /**
     * Create mailer
     *
     * @return mixed
     */
    protected static function createMailer() {

        //Lets get the current inputs so we can test them
        $input = JFactory::getApplication()->input;

        $smtpauth = $input->get('smtpauth');
        $smtpuser = $input->get('smtpuser', '', 'STRING');
        $smtppass = $input->get('smtppass', '', 'RAW');
        $smtphost = $input->get('smtphost');
        $smtpsecure = $input->get('smtpsecure');
        $smtpport = $input->get('smtpport');
        $mailfrom = $input->get('from_email', '', 'STRING');
        $fromname = $input->get('from_name', '', 'STRING');
        $mailer = $input->get('mailer');
        
        //Create a JMail object
        $mail = JFactory::getMailer();

        //Sender and Recipient
        $sender = array($mailfrom, $fromname);
        $mail->setSender($sender);
        $mail->addRecipient($sender);

        //Subject
        $subject = JText::_('PLG_CWMAILCHECK_EMAIL_SUBJECT');
        $mail->setSubject($subject);
        
        //Body
        $date = JDate::getInstance()->toSql();
        $body = JText::sprintf('PLG_CWMAILCHECK_EMAIL_BODY', $date);
        $mail->Encoding = 'base64';
        $mail->IsHTML(false);
        $mail->setBody($body);

        // Default mailer is to use PHP's mail function
        switch ($mailer) {
            case 'smtp':
                $mail->useSMTP($smtpauth, $smtphost, $smtpuser, $smtppass, $smtpsecure, $smtpport);
                break;

            case 'sendmail':
                $mail->IsSendmail();
                break;
            
            case 'mail':
                $mail->IsMail();
                break;

            default:
                $mail->IsMail();
                break;
        }

        return $mail;
    }

    /**
     * Create our Json encoded error info
     */
    protected function _error() {
        $result['status'] = 'error';
        $result['title'] = JText::_('PLG_CWMAILCHECK_TITLE_ERROR');
        $result['message'] = JText::_('PLG_CWMAILCHECK_MESSAGE_ERROR');
        $result = json_encode($result);

        echo $result;
        exit();
    }

    /**
     * Create our Json encoded success info
     */
    protected function _success() {
        $result['status'] = 'success';
        $result['title'] = JText::_('PLG_CWMAILCHECK_TITLE_SUCCESS');
        $result['message'] = JText::_('PLG_CWMAILCHECK_MESSAGE_SUCCESS');
        $result = json_encode($result);

        echo $result;
        exit();
    }

    /**
     * Lets add our scripts to do the heavy lifting
     *
     * @return string
     */
    private function _mailcheckScripts() {
        $root = JURI::root();
        $foo = '<span style=\"margin:20px;\">'
                . '<button class=\"btn\" type=\"button\" id=\"cw-mail-check\">'
                . '<i class=\"icon-envelope-opened\"></i>&nbsp;CoalaWeb Mail Check'
                . '</button></span>';

        $html = '<script type="text/javascript">
            (function ($) {
                $(document).ready(function () {
                    $("#jform_mailer-lbl").closest("fieldset").find("legend").append("' . $foo . '");   
                    $("#cw-mail-check").click(function () {
                        var smtp_auth = 1;
                        if ($("#jform_smtpauth1").prop("checked")) {
                            smtp_auth = 0;
                        }

                        var url = "' . $root . '";
                        url = url + "administrator/index.php?option=com_config&";
                        url = url + "plg_cwmailcheck=1";

                        var from_email = $("#jform_mailfrom").val();
                        var mailer = $("#jform_mailer :selected").val();
                        var from_name = $("#jform_fromname").val();
                        var send_path = $("#jform_sendmail").val();
                        var smtp_secure = $("#jform_smtpsecure :selected").val();
                        var smtp_port = $("#jform_smtpport").val();
                        var smtp_user = $("#jform_smtpuser").val();
                        var smtp_pass = $("#jform_smtppass").val();
                        var smtp_host = $("#jform_smtphost").val();

                        $.post(url,
                                {
                                    from_email: from_email,
                                    mailer: mailer,
                                    from_name: from_name,
                                    send_path: send_path,
                                    smtp_auth: smtp_auth,
                                    smtp_secure: smtp_secure,
                                    smtp_port: smtp_port,
                                    smtp_user: smtp_user,
                                    smtp_pass: smtp_pass,
                                    smtp_host: smtp_host
                                },
                        function (data, status) {
                            var record = JSON.parse(data);


                            if (record.status === "success") {
                                swal({
                                  title: record.title,
                                  text: record.message,
                                  type: record.status,
                                  html: true
                                });
                            }

                            if (record.status === "error") {
                               swal({
                                  title: record.title,
                                  text: record.message,
                                  type: record.status,
                                  html: true
                                });
                            }

                        });
                    });   
                });
            })(jQuery);
        </script>';

        return $html;
    }

    /**
     * Fetches the com_coalawebcontact component's parameters as a JRegistry instance
     *
     * @param $component
     * @return JRegistry The component parameters
     */
    private function getComponentParameters($component)
    {
        JLoader::import('joomla.registry.registry');
        $component = JComponentHelper::getComponent($component);

        if ($component->params instanceof JRegistry)
        {
            $cparams = $component->params;
        }
        elseif (!empty($component->params))
        {
            $cparams = new JRegistry($component->params);
        }
        else
        {
            $cparams = new JRegistry('{}');
        }

        return $cparams;
    }

    /**
     * Check dependencies
     *
     * @return array
     */
    private function checkDependencies()
    {

        $langRoot = 'PLG_CWMAILCHECK';

        if (!defined('COM_CWCONTACT_VERSION')) {
            $result = [
                'ok' => false,
                'type' => 'warning',
                'msg' => JText::_($langRoot . '_FILE_MISSING_MESSAGE')
            ];

            return $result;
        }

        /**
         * Gears dependencies
         */
        $version = (COM_CWCONTACT_MIN_GEARS_VERSION); // Minimum version

        // Classes that are needed
        $assets = [
            'mobile' => false,
            'count' => false,
            'tools' => true,
            'latest' => false
        ];

        // Check if Gears dependencies are meet and return result
        $results = self::checkGears($version, $assets, $langRoot);

        if ($results['ok'] == false) {
            $result = [
                'ok' => $results['ok'],
                'type' => $results['type'],
                'msg' => $results['msg']
            ];

            return $result;
        }


        // Lets use our tools class from Gears
        $tools = new CwGearsHelperTools();

        /**
         * File and folder dependencies
         * Note: JPATH_ROOT . '/' prefix will be added to file and folder names
         */
        $filesAndFolders = array(
            'files' => array(
            ),
            'folders' => array(
            )
        );

        // Check if they are available
        $exists = $tools::checkFilesAndFolders($filesAndFolders, $langRoot);

        // If any of the file/folder dependencies fail return
        if ($exists['ok'] == false) {
            $result = [
                'ok' => $exists['ok'],
                'type' => $exists['type'],
                'msg' => $exists['msg']
            ];

            return $result;
        }

        /**
         * Extension Dependencies
         * Note: Plugins always need to be entered in the following format plg_type_name
         */
        $extensions = array(
            'components' => array(
                'com_coalawebcontact'
            ),
            'modules' => array(
            ),
            'plugins' => array(
            )
        );

        // Check if they are available
        $extExists = $tools::checkExtensions($extensions, $langRoot);

        // If any of the extension dependencies fail return
        if ($extExists['ok'] == false) {
            $result = [
                'ok' => $extExists['ok'],
                'type' => $extExists['type'],
                'msg' => $extExists['msg']
            ];

            return $result;
        }

        // No problems? return all good
        $result = ['ok' => true];

        return $result;
    }

    /**
     * Check Gears dependencies
     *
     * @param $version - minimum version
     * @param array $assets - list of required assets
     * @param $langRoot
     * @return array
     */
    private function checkGears($version, $assets = array(), $langRoot)
    {
        jimport('joomla.filesystem.file');

        // Load the version.php file for the CW Gears plugin
        $version_gears_php = JPATH_SITE . '/plugins/system/cwgears/version.php';
        if (!defined('PLG_CWGEARS_VERSION') && JFile::exists($version_gears_php)) {
            include_once $version_gears_php;
        }

        // Is Gears installed and the right version and published?
        if (
            JPluginHelper::isEnabled('system', 'cwgears') &&
            JFile::exists($version_gears_php) &&
            version_compare(PLG_CWGEARS_VERSION, $version, 'ge')
        ) {
            // Base helper directory
            $helperDir = JPATH_SITE . '/plugins/system/cwgears/helpers/';

            // Do we need the mobile detect class?
            if ($assets['mobile'] == true && !class_exists('Cwmobiledetect')) {
                $mobiledetect_php = $helperDir . 'cwmobiledetect.php';
                if (JFile::exists($mobiledetect_php)) {
                    JLoader::register('Cwmobiledetect', $mobiledetect_php);
                } else {
                    $result = [
                        'ok' => false,
                        'type' => 'warning',
                        'msg' => JText::_($langRoot . '_NOGEARSPLUGIN_HELPER_MESSAGE')
                    ];
                    return $result;
                }
            }

            // Do we need the load count class?
            if ($assets['count'] == true && !class_exists('CwGearsHelperLoadcount')) {
                $loadcount_php = $helperDir . 'loadcount.php';
                if (JFile::exists($loadcount_php)) {
                    JLoader::register('CwGearsHelperLoadcount', $loadcount_php);
                } else {
                    $result = [
                        'ok' => false,
                        'type' => 'warning',
                        'msg' => JText::_($langRoot . '_NOGEARSPLUGIN_HELPER_MESSAGE')
                    ];
                    return $result;
                }
            }

            // Do we need the tools class?
            if ($assets['tools'] == true && !class_exists('CwGearsHelperTools')) {
                $tools_php = $helperDir . 'tools.php';
                if (JFile::exists($tools_php)) {
                    JLoader::register('CwGearsHelperTools', $tools_php);
                } else {
                    $result = [
                        'ok' => false,
                        'type' => 'warning',
                        'msg' => JText::_($langRoot . '_NOGEARSPLUGIN_HELPER_MESSAGE')
                    ];
                    return $result;
                }
            }

            // Do we need the latest class?
            if ($assets['latest'] == true && !class_exists('CwGearsLatestversion')) {
                $latest_php = $helperDir . 'latestversion.php';
                if (JFile::exists($latest_php)) {
                    JLoader::register('CwGearsLatestversion', $latest_php);
                } else {
                    $result = [
                        'ok' => false,
                        'type' => 'warning',
                        'msg' => JText::_($langRoot . '_NOGEARSPLUGIN_HELPER_MESSAGE')
                    ];
                    return $result;
                }
            }
        } else {
            // Looks like Gears isn't meeting the requirements
            $result = [
                'ok' => false,
                'type' => 'warning',
                'msg' => JText::sprintf($langRoot . '_NOGEARSPLUGIN_CHECK_MESSAGE', $version)
            ];
            return $result;
        }

        // Set up our response array
        $result = [
            'ok' => true,
            'type' => '',
            'msg' => ''
        ];

        // Return our result
        return $result;

    }
}