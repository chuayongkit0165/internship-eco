<?php

/**
 * @package     Joomla
 * @subpackage  CoalaWeb Contact
 * @author      Steven Palmer <support@coalaweb.com>
 * @link        https://coalaweb.com/
 * @license     GNU/GPL V3 or later; https://www.gnu.org/licenses/gpl-3.0.html
 * @copyright   Copyright (c) 2020 Steven Palmer All rights reserved.
 *
 * CoalaWeb Contact is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.filesystem.path');
jimport('joomla.filesystem.file');

use Joomla\Registry\Registry;
use Joomla\String\StringHelper;

/**
 * Class CoalaWebEmailHelper
 */
class CoalaWebEmailHelper
{
    /**
     * Create redirect
     *
     * @param $samePageUrl
     * @param $params
     * @return string
     */
    function getRedirect($samePageUrl, $params) {
        $homeUrl = JRoute::_(JURI::root());
        switch ($params['redirectUrl']) {
            case 1: $sucessUrl = ($samePageUrl . '#' . $params['sysMsg']);
                break;
            case 2: $sucessUrl = ($homeUrl . '#' . $params['sysMsg']);
                break;
            case 3: $sucessUrl = ($params['customUrl'] . '#' . $params['sysMsg']);
                break;
            default: $sucessUrl = JRoute::_(JURI::root());
                break;
        }

        return $sucessUrl;
    }

    /**
     * Check subject
     *
     * @param $subject
     * @param $params
     * @return mixed
     */
    function checkSubject($subject, $params)
    {
        $arr = array();
        $arr['subjectError'] = null;
        $arr['hasError'] = false;

        if ($params['displaySubject'] != 'R') {
            return $arr;
        }

        if (!$subject) {
            $arr['subjectError'] = $params['msgSubjectMissing'];
            $arr['hasError'] = true;
        }

        return $arr;
    }

    /**
     * Check basic Captcha
     *
     * @param $captcha
     * @param $params
     * @return array
     */
    function checkCaptcha($captcha, $params){
        $arr = array();
        if (!$captcha) {
            $arr['captchaError'] = $params['msgCaptchaWrong'];
            $arr['hasError'] = true;
        } elseif ($this->checkPass($params['bCaptchaAnswer'], $captcha)) {
            $arr['captchaError'] = $params['msgCaptchaWrong'];
            $arr['hasError'] = true;
        } else {
            $arr['captchaError'] = null;
            $arr['hasError'] = false;
        }
        return $arr;
    }

    /**
     * Check name
     *
     * @param $name
     * @param $params
     * @return mixed
     */
    function checkName($name, $params)
    {
        $arr = array();
        $arr['nameError'] = null;
        $arr['hasError'] = false;

        if ($params['displayName'] != 'R') {
            return $arr;
        }

        if (!$name) {
            $arr['nameError'] = $params['msgNameMissing'];
            $arr['hasError'] = true;
        }

        return $arr;

    }

    /**
     * Check message
     *
     * @param $message
     * @param $params
     * @return mixed
     */
    function checkMessage($message, $params)
    {
        $arr = array();
        $arr['messageError'] = null;
        $arr['hasError'] = false;

        if ($params['displayMessage'] != 'R') {
            return $arr;
        }

        if (!$message) {
            $arr['messageError'] = $params['msgMessageMissing'];
            $arr['hasError'] = true;
        }

        return $arr;

    }

    /**
     * Check email
     *
     * @param $email
     * @param $params
     * @return mixed
     */
    function checkEmail($email, $params)
    {
        $arr = array();
        $arr['emailError'] = null;
        $arr['hasError'] = false;

        if ($params['displayEmail'] == 'R') {
            if (!$email) {
                $arr['emailError'] = $params['msgEmailMissing'];
                $arr['hasError'] = true;
            } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $arr['emailError'] = $params['msgEmailInvalid'];
                $arr['hasError'] = true;
            }
        } else if ($params['displayEmail'] == 'Y' && $email) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $arr['emailError'] = $params['msgEmailInvalid'];
                $arr['hasError'] = true;
            }
        }
        return $arr;
    }

    /**
     * Check if any entered data is banned from use
     *
     * @param $banned
     * @param $checkValue
     * @return bool
     */
    private function checkAgainst($banned, $checkValue)
    {
        if ($banned)
        {
            foreach (explode(';', $banned) as $item)
            {
                if ($item != '' && StringHelper::stristr($checkValue, $item) !== false)
                {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Check if any entered data is banned from use
     *
     * @param $pass
     * @param $checkValue
     * @return bool
     */
    private function checkPass($pass, $checkValue)
    {

        $checkValue = stripcslashes($checkValue);
        if ($pass) {
            foreach (explode(';', $pass) as $item) {
                if ($item != '' && strcasecmp(trim($checkValue), trim($item)) == 0) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Check custom input
     *
     * @param $cInput1
     * @param $params
     * @return mixed
     */
    function checkCInput1($cInput1, $params)
    {
        $arr = array();
        $arr['cInput1Error'] = null;
        $arr['hasError'] = false;

        if ($params['displayCInput1'] != 'R') {
            return $arr;
        }

        if (!$cInput1) {
            $arr['cInput1Error'] = $params['msgCInput1Missing'];
            $arr['hasError'] = true;
        }

        return $arr;
    }

    /**
     * Create our custom select lists based on CSV list
     *
     * @param $selectCInput
     * @return string
     */
    function selectOptions($selectCInput) {
        $comParams = JComponentHelper::getParams('com_coalawebcontact');
        $delimiter = $comParams->get('custom_delimiter', ',');

        $arr = explode($delimiter, $selectCInput);
        foreach ($arr as $key => $opt) {
            $txt = $opt;
            $opt = preg_replace('/\s+/', '_', $opt);
            $arr[$key] = '<option value=' . $opt . '>' . $txt . '</option>';
        }
        $opts = implode(PHP_EOL, $arr);
        return $opts;
    }

    /**
     * Clean and tidy text
     *
     * @param $text
     * @param bool $stripHtml
     * @param $limit
     * @return mixed|Tidy
     */
    public static function textClean($text, $stripHtml = true, $limit)
    {
        // Now decoded the text
        $decoded = html_entity_decode($text);

        // Remove any HTML based on module settings
        $notags = $stripHtml ? strip_tags($decoded) : $decoded;

        // Remove brackets such as plugin code
        $nobrackets = preg_replace("/\{[^}]+\}/", " ", $notags);

        //Now reduce the text length if needed
        $chars = strlen($notags);
        if ($chars <= $limit) {
            $description = $nobrackets;
        } else {
            $description = JString::substr($nobrackets, 0, $limit) . "...";
        }

        // One last little clean up
        $cleanText = preg_replace("/\s+/", " ", $description);

        // Lastly repair any HTML that got cut off if Tidy is installed
        if (extension_loaded('tidy') && !$stripHtml) {
            $tidy = new Tidy();
            $config = array(
                'output-xml' => true,
                'input-xml' => true,
                'clean' => false
            );
            $tidy->parseString($cleanText, $config, 'utf8');
            $tidy->cleanRepair();
            $cleanText = $tidy;
        }

        return $cleanText;
    }

    /**
     * Clean and minimize code
     *
     * @param $code
     * @return string
     */
    public static function cleanCode($code) {

        // Remove comments.
        $pass1 = preg_replace('~//<!\[CDATA\[\s*|\s*//\]\]>~', '', $code);
        $pass2 = preg_replace('/(?:(?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:(?<!\:|\\\)\/\/[^"\'].*))/', '', $pass1);

        // Minimize.
        $pass3 = str_replace(array("\r\n", "\r", "\n", "\t"), '', $pass2);
        $pass4 = preg_replace('/ +/', ' ', $pass3); // Replace multiple spaces with single space.
        $codeClean = trim($pass4);  // Trim the string of leading and trailing space.

        return $codeClean;
    }

    /**
     * Build the email body
     *
     * @param $fields
     * @param $fromUrl
     * @param $params
     * @return string
     */
    function emailBody($fields, $fromUrl, $params) {

        // Setup email body text 
        switch ($params['emailFormat']) {
            case "nohtml":
                $body = '';

                if ($params['displayMailRequestA']) {
                    $body .= $params['reqTitleLbl'] . "\r\n";
                    $body .= $fields['subject'] ? $params['subjectLbl'] . ": " . $fields['subject'] . "\r\n" : "";
                    $body .= $fields['message'] ? $params['messageLbl'] . ": " . $fields['message'] . "\r\n" : "";
                    $body .= "\r\n";
                }

                if ($params['displayMailSentbyA']) {
                    $body .= $params['sByTitleLbl'] . "\r\n";
                    $body .= $fields['name'] ? $params['nameLbl'] . ": " . $fields['name'] . "\r\n" : "";
                    $body .= $fields['email'] ? $params['emailLbl'] . ": " . $fields['email'] . "\r\n" : "";
                    $body .= (isset($fields['cInput1']) && $fields['cInput1']) ? $params['cInput1Lbl'] . ": " . str_replace('_', ' ', $fields['cInput1']) . "\r\n" : "";
                    $body .= "\r\n";
                }

                if ($params['displayMailSentfromA']) {
                    $body .= $params['sFromTitleLbl'] . "\r\n";
                    $body .= $fields['remoteHost'] ? $params['sByIpLbl'] . ": " . $fields['remoteHost'] . "\r\n" : "";
                    $body .= $params['sFromWebLbl'] . ": " . $params['sitename'] . "\r\n";
                    $body .= $fromUrl ? $params['sFromUrlLbl'] . ": " . $fromUrl : "";
                    $body .= "\r\n";
                }
                break;

        }

        return $body;
    }

    /**
     * Build copy me email
     *
     * @param $fields
     * @param $fromUrl
     * @param $params
     * @return string
     */
    function copymeBody($fields, $fromUrl, $params) {

        // Setup email body text 
        switch ($params['emailFormat']) {
            case "nohtml":
                $body = '';

                if ($params['displayMailRequestC']) {
                    $body .= $params['reqTitleLbl'] . "\r\n";
                    $body .= $fields['subject'] ? $params['subjectLbl'] . ": " . $fields['subject'] . "\r\n" : "";
                    $body .= $fields['message'] ? $params['messageLbl'] . ": " . $fields['message'] . "\r\n" : "";
                    $body .= "\r\n";
                }

                if ($params['displayMailSentbyC']) {
                    $body .= $params['sByTitleLbl'] . "\r\n";
                    $body .= $fields['name'] ? $params['nameLbl'] . ": " . $fields['name'] . "\r\n" : "";
                    $body .= $fields['email'] ? $params['emailLbl'] . ": " . $fields['email'] . "\r\n" : "";
                    $body .= (isset($fields['cInput1']) && $fields['cInput1']) ? $params['cInput1Lbl'] . ": " . str_replace('_', ' ', $fields['cInput1']). "\r\n" : "";
                    $body .= "\r\n";
                }

                if ($params['displayMailSentfromC']) {
                    $body .= $params['sFromTitleLbl'] . "\r\n";
                    $body .= $fields['remoteHost'] ? $params['sByIpLbl'] . ": " . $fields['remoteHost'] . "\r\n" : "";
                    $body .= $params['sFromWebLbl'] . ": " . $params['sitename'] . "\r\n";
                    $body .= $fromUrl ? $params['sFromUrlLbl'] . ": " . $fromUrl : "";
                    $body .= "\r\n";
                }
                break;

        }

        return $body;
    }

    /**
     * Check dependencies
     *
     * @return array
     */
    public static function checkDependencies() {

        $langRoot = 'COM_CWCONTACT';

        if (!defined('COM_CWCONTACT_VERSION')) {
            $result = [
                'ok' => false,
                'type' => 'warning',
                'msg' => JText::_($langRoot . '_FILE_MISSING_MESSAGE')
            ];

            return $result;
        }

        /**
         * Gears dependencies
         */
        $version = (COM_CWCONTACT_MIN_GEARS_VERSION); // Minimum version

        // Classes that are needed
        $assets = [
            'mobile' => false,
            'count' => true,
            'tools' => true,
            'latest' => true
        ];

        // Check if Gears dependencies are meet and return result
        $results = self::checkGears($version, $assets, $langRoot);

        if($results['ok'] == false){
            $result = [
                'ok' => $results['ok'],
                'type' => $results['type'],
                'msg' => $results['msg']
            ];

            return $result;
        }


        // Lets use our tools class from Gears
        $tools = new CwGearsHelperTools();

        /**
         * File and folder dependencies
         * Note: JPATH_ROOT . '/' prefix will be added to file and folder names
         */
        $filesAndFolders = array(
            'files' => array(
            ),
            'folders' => array(
            )
        );

        // Check if they are available
        $exists = $tools::checkFilesAndFolders($filesAndFolders, $langRoot);

        // If any of the file/folder dependencies fail return
        if($exists['ok'] == false){
            $result = [
                'ok' => $exists['ok'],
                'type' => $exists['type'],
                'msg' => $exists['msg']
            ];

            return $result;
        }

        /**
         * Extension Dependencies
         * Note: Plugins always need to be entered in the following format plg_type_name
         */
        $extensions = array(
            'components' => array(
            ),
            'modules' => array(
            ),
            'plugins' => array(
            )
        );

        // Check if they are available
        $extExists = $tools::checkExtensions($extensions, $langRoot);

        // If any of the extension dependencies fail return
        if($extExists['ok'] == false){
            $result = [
                'ok' => $extExists['ok'],
                'type' => $extExists['type'],
                'msg' => $extExists['msg']
            ];

            return $result;
        }

        // No problems? return all good
        $result = ['ok' => true];

        return $result;
    }

    /**
     * Check Gears dependencies
     *
     * @param $version - minimum version
     * @param array $assets - list of required assets
     * @param $langRoot
     * @return array
     */
    public static function checkGears($version, $assets = array(), $langRoot)
    {
        jimport('joomla.filesystem.file');

        // Load the version.php file for the CW Gears plugin
        $version_php = JPATH_SITE . '/plugins/system/cwgears/version.php';
        if (!defined('PLG_CWGEARS_VERSION') && JFile::exists($version_php)) {
            include_once $version_php;
        }

        // Is Gears installed and the right version and published?
        if (
            JPluginHelper::isEnabled('system', 'cwgears') &&
            JFile::exists($version_php) &&
            version_compare(PLG_CWGEARS_VERSION, $version, 'ge')
        ) {
            // Base helper directory
            $helperDir = JPATH_SITE . '/plugins/system/cwgears/helpers/';

            // Do we need the mobile detect class?
            if ($assets['mobile'] == true && !class_exists('Cwmobiledetect')) {
                $mobiledetect_php = $helperDir . 'cwmobiledetect.php';
                if (JFile::exists($mobiledetect_php)) {
                    JLoader::register('Cwmobiledetect', $mobiledetect_php);
                } else {
                    $result = [
                        'ok' => false,
                        'type' => 'notice',
                        'msg' => JText::_($langRoot . '_NOGEARSPLUGIN_HELPER_MESSAGE')
                    ];
                    return $result;
                }
            }

            // Do we need the load count class?
            if ($assets['count'] == true && !class_exists('CwGearsHelperLoadcount')) {
                $loadcount_php = $helperDir . 'loadcount.php';
                if (JFile::exists($loadcount_php)) {
                    JLoader::register('CwGearsHelperLoadcount', $loadcount_php);
                } else {
                    $result = [
                        'ok' => false,
                        'type' => 'notice',
                        'msg' => JText::_($langRoot . '_NOGEARSPLUGIN_HELPER_MESSAGE')
                    ];
                    return $result;
                }
            }

            // Do we need the tools class?
            if ($assets['tools'] == true && !class_exists('CwGearsHelperTools')) {
                $tools_php = $helperDir . 'tools.php';
                if (JFile::exists($tools_php)) {
                    JLoader::register('CwGearsHelperTools', $tools_php);
                } else {
                    $result = [
                        'ok' => false,
                        'type' => 'notice',
                        'msg' => JText::_($langRoot . '_NOGEARSPLUGIN_HELPER_MESSAGE')
                    ];
                    return $result;
                }
            }

            // Do we need the latest class?
            if ($assets['latest'] == true && !class_exists('CwGearsLatestversion')) {
                $latest_php = $helperDir . 'latestversion.php';
                if (JFile::exists($latest_php)) {
                    JLoader::register('CwGearsLatestversion', $latest_php);
                } else {
                    $result = [
                        'ok' => false,
                        'type' => 'notice',
                        'msg' => JText::_($langRoot . '_NOGEARSPLUGIN_HELPER_MESSAGE')
                    ];
                    return $result;
                }
            }
        } else {
            // Looks like Gears isn't meeting the requirements
            $result = [
                'ok' => false,
                'type' => 'notice',
                'msg' => JText::sprintf($langRoot . '_NOGEARSPLUGIN_CHECK_MESSAGE', $version)
            ];
            return $result;
        }

        // Set up our response array
        $result = [
            'ok' => true,
            'type' => '',
            'msg' => ''
        ];

        // Return our result
        return $result;

    }

}
