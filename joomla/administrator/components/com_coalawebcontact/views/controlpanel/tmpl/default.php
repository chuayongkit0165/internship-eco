<?php

/**
 * @package     Joomla
 * @subpackage  CoalaWeb Contact
 * @author      Steven Palmer <support@coalaweb.com>
 * @link        https://coalaweb.com/
 * @license     GNU/GPL V3 or later; https://www.gnu.org/licenses/gpl-3.0.html
 * @copyright   Copyright (c) 2020 Steven Palmer, All rights reserved.
 *
 * CoalaWeb Contact is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

defined('_JEXEC') or die('Restricted access');

JHtml::_('jquery.framework');

$user = JFactory::getUser();
$lang = JFactory::getLanguage();

use CoalaWeb\Messages as CW_Messages;

$component = json_decode($this->component->manifest_cache);
?>

<div id="cpanel-v2" class="span8">
    <div class="row-fluid well">

        <div style="float:<?php echo ($lang->isRTL()) ? 'right' : 'left'; ?>;">
            <div class="icon">
                <a class="blue-light" href="index.php?option=com_config&view=component&component=com_coalawebcontact">
                    <img alt="<?php echo JText::_('COM_CWCONTACT_TITLE_OPTIONS'); ?>"
                         src="<?php echo JURI::root() ?>media/coalaweb/components/generic/images/icons/icon-48-cw-options-v2.png"/>
                    <span><?php echo JText::_('COM_CWCONTACT_TITLE_OPTIONS'); ?></span>
                </a>
            </div>
        </div>

        <div style="float:<?php echo ($lang->isRTL()) ? 'right' : 'left'; ?>;">
            <div class="icon">
                <a class="red-light"
                   onclick="Joomla.popupWindow('https://coalaweb.com/support/documentation/item/coalaweb-contact-guide', 'Help', 700, 500, 1);"
                   href="#">
                    <img alt="<?php echo JText::_('COM_CWCONTACT_TITLE_HELP'); ?>"
                         src="<?php echo JURI::root() ?>media/coalaweb/components/generic/images/icons/icon-48-cw-support-v2.png"/>
                    <span><?php echo JText::_('COM_CWCONTACT_TITLE_HELP'); ?></span>
                </a>
            </div>
        </div>

        <div style="float:<?php echo ($lang->isRTL()) ? 'right' : 'left'; ?>;">
            <div class="icon">
                <a class="pink-light"
                   onclick="Joomla.popupWindow('https://coalaweb.com/extensions/joomla-extensions/coalaweb-contact/feature-comparison', 'Comparison', 700, 500, 1)"
                   href="#">
                    <img alt="<?php echo JText::_('COM_CWCONTACT_TITLE_UPGRADE'); ?>"
                         src="<?php echo JURI::root() ?>media/coalaweb/components/generic/images/icons/icon-48-cw-upgrade-v2.png"/>
                    <span><?php echo JText::_('COM_CWCONTACT_TITLE_UPGRADE'); ?></span>
                </a>
            </div>
        </div>

    </div>

    <div class="row-fluid">
        <div class="well well-small span6">

            <h3 class="module-title nav-header"><?php echo JText::_('COM_CWCONTACT_TITLE_RELATED_MODULES') ?></h3>
            <div class="row-striped">
                <?php foreach ($this->status->modules as $module) : ?>
                    <div class="row-fluid">

                        <div class="span6">
                            <?php echo JText::_($module['name']); ?>
                        </div>
                        <div class="span3">
                            <?php echo ucfirst($module['client']); ?>
                        </div>
                        <div class="span1">
                            <?php if ($module['enabled']) echo '<span class="icon-publish"></span>';
                            if (!$module['enabled']) echo '<span class="icon-unpublish"></span>';
                            ?>
                        </div>
                        <?php if ($module['installed']) : ?>
                            <div class="span2">
                                <form action="<?php echo JRoute::_("index.php?option=com_installer&view=manage") ?>"
                                      method="post" id="pluginform" class="remove-all-margin">
                                    <input type="hidden" name="filter_search" id="filter_search"
                                           value="ID:<?php echo JText::_($module['id']); ?>" title=""/>
                                    <input type="submit" class="btn btn-mini btn-primary"
                                           value="<?php echo JText::_('COM_CWCONTACT_BTN_MANAGE') ?>">
                                </form>
                            </div>
                        <?php else : ?>
                            <div class="span2">
                                <a href="https://coalaweb.com/support/documentation/item/coalaweb-contact-system-parts"
                                   class="btn btn-mini btn-danger" target="_blank">
                                    <?php echo JText::_('COM_CWCONTACT_BTN_MISSING'); ?>
                                </a>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            </div>

        </div>

        <div class="well well-small span6">

            <h3 class="module-title nav-header"><?php echo JText::_('COM_CWCONTACT_TITLE_RELATED_PLUGINS') ?></h3>
            <div class="row-striped">
                <?php foreach ($this->status->plugins as $plugin) : ?>
                    <div class="row-fluid">

                        <div class="span6">
                            <?php echo JText::_($plugin['name']); ?>
                        </div>
                        <div class="span3">
                            <?php echo ucfirst($plugin['group']); ?>
                        </div>
                        <div class="span1">
                            <?php if ($plugin['enabled']) echo '<span class="icon-publish"></span>';
                            if (!$plugin['enabled']) echo '<span class="icon-unpublish"></span>';
                            ?>
                        </div>
                        <?php if ($plugin['installed']) : ?>
                            <div class="span2">
                                <form action="<?php echo JRoute::_("index.php?option=com_installer&view=manage") ?>"
                                      method="post" id="pluginform" class="remove-all-margin">
                                    <input type="hidden" name="filter_search" id="filter_search"
                                           value="ID:<?php echo JText::_($plugin['id']); ?>" title=""/>
                                    <input type="submit" class="btn btn-mini btn-primary"
                                           value="<?php echo JText::_('COM_CWCONTACT_BTN_MANAGE') ?>">
                                </form>
                            </div>
                        <?php else : ?>
                            <div class="span2">
                                <a href="https://coalaweb.com/support/documentation/item/coalaweb-contact-system-parts"
                                   class="btn btn-mini btn-danger" target="_blank">
                                    <?php echo JText::_('COM_CWCONTACT_BTN_MISSING'); ?>
                                </a>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            </div>

        </div>
    </div>
</div>

<div id="tabs" class="span4">
    <div class="row-fluid">

        <?php
        $options = array(
            'onActive' => 'function(title, description){
        description.setStyle("display", "block");
        title.addClass("open").removeClass("closed");
    }',
            'onBackground' => 'function(title, description){
        description.setStyle("display", "none");
        title.addClass("closed").removeClass("open");
    }',
            'startOffset' => 0, // 0 starts on the first tab, 1 starts the second, etc...
            'useCookie' => true, // this must not be a string. Don't use quotes.
            'startTransition' => 1,
        );
        ?>

        <?php echo JHtml::_('sliders.start', 'slider_group_id', $options); ?>

        <?php echo JHtml::_('sliders.panel', JText::_('COM_CWCONTACT_SLIDER_TITLE_ABOUT'), 'slider_1_id'); ?>
        <div class="well well-large">

            <div class="center">
                <h1><?php echo JText::_('COM_CWCONTACT_TITLE_CORE'); ?></h1>
            </div>

            <?php echo CW_Messages::getInstance()->getMessage('info',  JText::_('COM_CWCONTACT_ABOUT_DESCRIPTION')); ?>

            <dl class="dl-horizontal">
                <hr class="hr-condensed">
                <dt><?php echo JText::_('COM_CWCONTACT_FIELD_RELEASE_CURRENT_LABEL') ?></dt>
                <dd><?php echo $component->version . ' ' . $this->proCore ?></dd>
                <hr class="hr-condensed">
                <dt><?php echo JText::_('COM_CWCONTACT_FIELD_RELEASE_LATEST_LABEL') ?></dt>
                <?php
                $layout = new JLayoutFile('label_unknown');
                if($this->component->new_version == '' || $this->component->new_version < $component->version){
                    $is_new = $component->version . ' ' . $this->proCore;
                } else{
                    $is_new =  '<span class="label label-danger label-important">' . $this->component->new_version . ' ' . $this->proCore . '</span>';
                }
                echo '<dd>' . $is_new  . '</dd>';
                ?>
                <hr class="hr-condensed">
                <dt><?php echo JText::_('COM_CWCONTACT_FIELD_RELEASE_DATE_LABEL') ?></dt>
                <dd><?php echo $component->creationDate; ?></dd>
                <hr class="hr-condensed">
                <dt>Website:</dt>
                <dd><?php echo '<a href=" ' . $component->authorUrl . '">' . parse_url($component->authorUrl, PHP_URL_HOST) . '</a>' ?></dd>
                <hr class="hr-condensed">
                <dt>License:</dt>
                <dd><?php echo $this->license; ?></dd>
                <hr class="hr-condensed">
                <dt>Copyright:</dt>
                <dd><?php echo $component->copyright; ?></dd>
                <hr class="hr-condensed">
            </dl>

            <?php echo JText::_('COM_CWCONTACT_JED_REVIEW'); ?>

        </div>

        <?php echo JHtml::_('sliders.panel', JText::_('COM_CWCONTACT_SLIDER_TITLE_UPGRADE'), 'slider_2_id'); ?>

        <div class="well well-large">

            <div class="center alert alert-info">
                <h2> <?php echo JText::_('COM_CWCONTACT_TITLE_UPGRADE') ?></h2>
                <?php echo JText::_('COM_CWCONTACT_MSG_UPGRADE') ?>
            </div>

            <div class="alert alert-success">
                <h3> <?php echo JText::_('COM_CWCONTACT_TITLE_DEMO') ?></h3>
                <?php echo JText::_('COM_CWCONTACT_MSG_DEMO'); ?>
            </div>

            <div class="alert alert-danger">
                <h3> <?php echo JText::_('COM_CWCONTACT_TITLE_PRICES') ?></h3>
                <?php echo JText::_('COM_CWCONTACT_MSG_PRICES'); ?>
            </div>

            <div class="alert alert-warning">
                <h3> <?php echo JText::_('COM_CWCONTACT_TITLE_COMPARISON') ?></h3>
                <?php echo JText::_('COM_CWCONTACT_MSG_COMPARISON'); ?>
            </div>

        </div>

        <?php echo JHtml::_('sliders.panel', JText::_('COM_CWCONTACT_SLIDER_TITLE_SUPPORT'), 'slider_3_id'); ?>

        <div class="well well-large">
            <?php echo JText::_('COM_CWCONTACT_SUPPORT_DESCRIPTION'); ?>
        </div>

        <?php echo JHtml::_('sliders.end'); ?>
    </div>
</div>