<?php
/********************************************************************
Product	   : Flexicontact
Date       : 27 January 2021
Copyright  : Les Arbres Design 2009-2021
Contact	   : http://www.lesarbresdesign.info
Licence	   : GNU General Public License
*********************************************************************/
defined('_JEXEC') or die('Restricted access');

class FlexicontactViewConfig extends JViewLegacy
{
function display($tpl = null)
{
	Flexicontact_Utility::viewStart();
	JToolBarHelper::title(LAFC_COMPONENT_NAME.': '.JText::_('COM_FLEXICONTACT_CONFIGURATION'), 'lad.png');
	if (JFactory::getUser()->authorise('core.admin', 'com_flexicontact'))
		JToolBarHelper::preferences('com_flexicontact');

// Set up the configuration links

	$config_table = array(
		array(
			'link' => LAFC_COMPONENT_LINK.'&task=config&function=edit_form&param1=config_general',
			'icon' => 'config_general.png',
			'name' => 'COM_FLEXICONTACT_CONFIG_GENERAL_NAME',
			'desc' => 'COM_FLEXICONTACT_CONFIG_GENERAL_DESC'),
		array(
			'link' => LAFC_COMPONENT_LINK.'&task=config&function=edit_form&param1=admin_template&vars=yes',
			'icon' => 'config_email_a.png',
			'name' => 'COM_FLEXICONTACT_CONFIG_ADMIN_EMAIL_NAME',
			'desc' => 'COM_FLEXICONTACT_CONFIG_ADMIN_EMAIL_DESC'),
		array(
			'link' => LAFC_COMPONENT_LINK.'&task=config&function=edit_form&param1=user_template&vars=yes',
			'icon' => 'config_email_u.png',
			'name' => 'COM_FLEXICONTACT_CONFIG_USER_EMAIL_NAME',
			'desc' => 'COM_FLEXICONTACT_CONFIG_USER_EMAIL_DESC'),
		array(
			'link' => LAFC_COMPONENT_LINK.'&task=config&function=edit_form&param1=confirm_text&vars=yes',
			'icon' => 'config_confirm.png',
			'name' => 'COM_FLEXICONTACT_CONFIG_CONFIRM_NAME',
			'desc' => 'COM_FLEXICONTACT_CONFIG_CONFIRM_DESC'),
		array(
			'link' => LAFC_COMPONENT_LINK.'&task=config&function=edit_form&param1=config_fields',
			'icon' => 'config_fields.png',
			'name' => 'COM_FLEXICONTACT_CONFIG_FIELDS_NAME',
			'desc' => 'COM_FLEXICONTACT_CONFIG_FIELDS_DESC'),
		array(
			'link' => LAFC_COMPONENT_LINK.'&task=config&function=edit_form&param1=page_text',
			'icon' => 'config_text_top.png',
			'name' => 'COM_FLEXICONTACT_V_TOP_TEXT',
			'desc' => 'COM_FLEXICONTACT_CONFIG_TOP_BOTTOM_TEXT_DESC'),
		array(
			'link' => LAFC_COMPONENT_LINK.'&task=config&function=edit_form&param1=bottom_text',
			'icon' => 'config_text_bottom.png',
			'name' => 'COM_FLEXICONTACT_V_BOTTOM_TEXT',
			'desc' => 'COM_FLEXICONTACT_CONFIG_TOP_BOTTOM_TEXT_DESC'),
		array(
			'link' => LAFC_COMPONENT_LINK.'&task=config&function=edit_form&param1=config_captcha',
			'icon' => 'config_captcha.png',
			'name' => 'COM_FLEXICONTACT_CAPTCHA_CONFIG',
			'desc' => 'COM_FLEXICONTACT_CAPTCHA_CONFIG_DESC'),
		array(
			'link' => LAFC_COMPONENT_LINK.'&task=config&function=edit_css',
			'icon' => 'config_css.png',
			'name' => 'COM_FLEXICONTACT_CONFIG_CSS_NAME',
			'desc' => 'COM_FLEXICONTACT_CONFIG_CSS_DESC')
		);

    $this->form();

	echo '<table class="table table-striped"><thead><tr>';
	echo '<th style="width:5%;"></th>';
	echo '<th style="width:20%; white-space:nowrap;">'.JText::_('COM_FLEXICONTACT_CONFIG_NAME').'</th>';
	echo '<th style="width:75%; white-space:nowrap;">'.JText::_('COM_FLEXICONTACT_CONFIG_DESC').'</th>';
	echo '</tr></thead>';

	foreach ($config_table as $config)
		{
		$icon = '<img src="'.LAFC_ADMIN_ASSETS_URL.$config['icon'].'" alt="" />';
		echo "<tr>
				<td>$icon</td>
				<td>".JHTML::link($config['link'], JText::_($config['name']))."</td>
				<td>".JText::_($config['desc'])."</td>
			</tr>";
		}
	echo '</table></form>';
	Flexicontact_Utility::viewEnd();
}

//-------------------------------------------------------------------------------
// Handle form-based config pages
//
function edit_form()
{
	Flexicontact_Utility::viewStart();
   	JToolBarHelper::title(LAFC_COMPONENT_NAME.': '.JText::_(self::get_title($this->param1)), 'lad.png');
	JToolBarHelper::apply();
	JToolBarHelper::save();
	JToolBarHelper::cancel();
    $this->form();
    
    if ($this->vars)
        {
		if (substr(JVERSION,0,1) == '3')
			$uitab = 'bootstrap';
		else
			$uitab = 'uitab';
        echo JHtml::_($uitab.'.startTabSet','config_tabs', array('active' => 'main'));
        echo JHtml::_($uitab.'.addTab', 'config_tabs', 'main', JText::_('COM_FLEXICONTACT_TEXT'));
        }

	$form = JForm::getInstance($this->param1, JPATH_ROOT.'/administrator/components/com_flexicontact/forms/'.$this->param1.'.xml');
	JForm::addFieldPath(JPATH_ROOT.'/administrator/components/com_flexicontact/forms');
	$field_sets = $form->getFieldsets();
    
	foreach ($field_sets as $fieldset_name => $fieldset)
        {
        if (isset($fieldset->class))
            echo '<fieldset class="'.$fieldset->class.'">';
        else
            echo '<fieldset>';
        if ($fieldset->label != '')
            echo '<legend>'.JText::_($fieldset->label).'</legend>';
            
        $form->bind($this->config_data);
        echo $form->renderFieldset($fieldset_name);
        echo '</fieldset>';
        }
        
    if ($this->vars)
        {
        echo JHtml::_($uitab.'.endTab');
        echo JHtml::_($uitab.'.addTab', 'config_tabs', 'variables', JText::_('COM_FLEXICONTACT_VARIABLES'));
        echo '<h5>'.JText::_('COM_FLEXICONTACT_CLICK_TO_COPY').'</h5>';
    	echo self::make_key_table();
        echo JHtml::_($uitab.'.endTab');
        echo JHtml::_($uitab.'.endTabSet');
        }        

	echo '</form>';
	Flexicontact_Utility::viewEnd();
}

//-------------------------------------------------------------------------------
// Create the Variables Key Table
//
static function make_key_table()
{
    JHtml::_('bootstrap.tooltip');
    if (substr(JVERSION,0,1) == '3')        // Bootstrap 2 needed a different way to alter the tooltip
        $js= "function fc_add(tag)
            {var tt=document.createElement('textarea'); tt.id='tt'; tt.style.height=0; document.body.appendChild(tt);
             tt.value=tag.innerHTML; document.getElementById('tt').select(); document.execCommand('copy'); document.body.removeChild(tt);
             jQuery('.hasTooltip').attr('title', '".JText::_('COM_FLEXICONTACT_COPY')."').tooltip('fixTitle');
             jQuery(tag).attr('title', '".JText::_('COM_FLEXICONTACT_COPIED')."').tooltip('fixTitle').tooltip('show');};\n";
    else
        $js= "function fc_add(tag)
            {var tt=document.createElement('textarea'); tt.id='tt'; tt.style.height=0; document.body.appendChild(tt);
             tt.value=tag.innerHTML; document.getElementById('tt').select(); document.execCommand('copy'); document.body.removeChild(tt);
             jQuery('.hasTooltip').attr('data-original-title', '".JText::_('COM_FLEXICONTACT_COPY')."');
             jQuery(tag).attr('data-original-title', '".JText::_('COM_FLEXICONTACT_COPIED')."').tooltip('show');};\n";
	$doc = JFactory::getDocument();
	$doc->addScriptDeclaration($js);
    
    $span_start = '<span onclick="fc_add(this)" class="hasTooltip lad-key" title="'.JText::_('COM_FLEXICONTACT_COPY').'">';
    $span_end   = '</span>';
    
	$html = '<table class="table table-striped table-bordered width-auto">';
    $html .= '<tr><th>'.JText::_('COM_FLEXICONTACT_FIELD').'</th><th>'.JText::_('COM_FLEXICONTACT_FIELD_NAME').'</th><th>'.JText::_('COM_FLEXICONTACT_FIELD_VALUE').'</th></tr>';

    $html .= '<tr><td>'.JText::_('COM_FLEXICONTACT_NAME').'</td><td></td><td class="lad-key-cell">'.$span_start.LAFC_T_FROM_NAME.$span_end.'</td></tr>';
    $html .= '<tr><td>'.JText::_('COM_FLEXICONTACT_EMAIL').'</td><td></td><td class="lad-key-cell">'.$span_start.LAFC_T_FROM_EMAIL.$span_end.'</td></tr>';
    $html .= '<tr><td>'.JText::_('COM_FLEXICONTACT_MESSAGE').'</td><td class="lad-key-cell">'.$span_start.LAFC_T_MESSAGE_PROMPT.$span_end.'</td><td class="lad-key-cell">'.$span_start.LAFC_T_MESSAGE_DATA.$span_end.'</td></tr>';
    $html .= '<tr><td>'.JText::_('COM_FLEXICONTACT_SUBJECT').'</td><td></td><td class="lad-key-cell">'.$span_start.LAFC_T_SUBJECT.$span_end.'</td></tr>';
    $html .= '<tr><td>'.JText::_('COM_FLEXICONTACT_LIST').'</td><td class="lad-key-cell">'.$span_start.LAFC_T_LIST_PROMPT.$span_end.'</td><td class="lad-key-cell">'.$span_start.LAFC_T_LIST_DATA.$span_end.'</td></tr>';
    $html .= '<tr><td>'.JText::_('COM_FLEXICONTACT_FIELD_1').'</td><td class="lad-key-cell">'.$span_start.LAFC_T_FIELD1_PROMPT.$span_end.'</td><td class="lad-key-cell">'.$span_start.LAFC_T_FIELD1_DATA.$span_end.'</td></tr>';
    $html .= '<tr><td>'.JText::_('COM_FLEXICONTACT_FIELD_2').'</td><td class="lad-key-cell">'.$span_start.LAFC_T_FIELD2_PROMPT.$span_end.'</td><td class="lad-key-cell">'.$span_start.LAFC_T_FIELD2_DATA.$span_end.'</td></tr>';
    $html .= '<tr><td>'.JText::_('COM_FLEXICONTACT_FIELD_3').'</td><td class="lad-key-cell">'.$span_start.LAFC_T_FIELD3_PROMPT.$span_end.'</td><td class="lad-key-cell">'.$span_start.LAFC_T_FIELD3_DATA.$span_end.'</td></tr>';
    $html .= '<tr><td>'.JText::_('COM_FLEXICONTACT_FIELD_4').'</td><td class="lad-key-cell">'.$span_start.LAFC_T_FIELD4_PROMPT.$span_end.'</td><td class="lad-key-cell">'.$span_start.LAFC_T_FIELD4_DATA.$span_end.'</td></tr>';
    $html .= '<tr><td>'.JText::_('COM_FLEXICONTACT_FIELD_5').'</td><td class="lad-key-cell">'.$span_start.LAFC_T_FIELD5_PROMPT.$span_end.'</td><td class="lad-key-cell">'.$span_start.LAFC_T_FIELD5_DATA.$span_end.'</td></tr>';
    $html .= '<tr><td>'.JText::_('COM_FLEXICONTACT_BROWSER').'</td><td></td><td class="lad-key-cell">'.$span_start.LAFC_T_BROWSER.$span_end.'</td></tr>';
    $html .= '<tr><td>'.JText::_('COM_FLEXICONTACT_IP_ADDRESS').'</td><td></td><td class="lad-key-cell">'.$span_start.LAFC_T_IP_ADDRESS.$span_end.'</td></tr>';
    $html .= '<tr><td>'.JText::_('COM_FLEXICONTACT_SITE_NAME').'</td><td></td><td class="lad-key-cell">'.$span_start.LAFC_T_SITE_NAME.$span_end.'</td></tr>';
 	$html .= '</table>';
    return $html;
}

//-------------------------------------------------------------------------------
// Edit the front end CSS file
//
function edit_css()
{
	JToolBarHelper::title(LAFC_COMPONENT_NAME.': '.JText::_('COM_FLEXICONTACT_CONFIG_CSS_NAME'), 'lad.png');
	JToolBarHelper::apply('apply_css');
	JToolBarHelper::save('save_css');
	JToolBarHelper::cancel();
	
	if (!file_exists(LAFC_SITE_CSS_PATH)) 
		{ 
		$app = JFactory::getApplication();
		$app->redirect(LAFC_COMPONENT_LINK.'&task=config',
			JText::_('COM_FLEXICONTACT_CSS_MISSING').' ('.LAFC_SITE_CSS_PATH.')', 'error');
		return;
		}
		
	if (!is_readable(LAFC_SITE_CSS_PATH)) 
		{ 
		$app = JFactory::getApplication();
		$app->redirect(LAFC_COMPONENT_LINK.'&task=config',
			JText::_('COM_FLEXICONTACT_CSS_NOT_READABLE').' ('.LAFC_SITE_CSS_PATH.')', 'error'); 
		return;
		}

	if (!is_writable(LAFC_SITE_CSS_PATH)) 
		{ 
		$app = JFactory::getApplication();
		$app->redirect(LAFC_COMPONENT_LINK.'&task=config',
			JText::_('COM_FLEXICONTACT_CSS_NOT_WRITEABLE').' ('.LAFC_SITE_CSS_PATH.')', 'error'); 
		return;
		}
		
	$css_contents = @file_get_contents(LAFC_SITE_CSS_PATH);

	Flexicontact_Utility::viewStart();
    $this->form();

    echo '<div class="width-auto" style="display:inline-block;">';
    echo '<textarea name="css_contents" rows="25" cols="125" class="lad-fixed-font lad-field-inline" style="width:100%">'.$css_contents.'</textarea>';
    echo '</div>';

	echo '</form>';
	Flexicontact_Utility::viewEnd();
}

//-------------------------------------------------------------------------------
// Output the form common to all config pages
//
function form()
{
	echo '<form action="index.php" method="post" name="adminForm" id="adminForm" class="form-horizontal">';
	echo '<input type="hidden" name="option" value="com_flexicontact" />';
	echo '<input type="hidden" name="task" value="" />';
	echo '<input type="hidden" name="function" value="'.$this->function.'" />';
	echo '<input type="hidden" name="param1" value="'.$this->param1.'" />';
	echo '<input type="hidden" name="vars" value="'.$this->vars.'" />';
}

//-------------------------------------------------------------------------------
// Map config functions to language strings for the page title
//
static function get_title($function)
{
    switch ($function)
        {
        case 'config_general':   return 'COM_FLEXICONTACT_CONFIG_GENERAL_NAME';
        case 'config_captcha':   return 'COM_FLEXICONTACT_CAPTCHA_CONFIG';
        case 'config_fields':    return 'COM_FLEXICONTACT_CONFIG_FIELDS_NAME';
        case 'user_template':    return 'COM_FLEXICONTACT_CONFIG_USER_EMAIL_NAME';
        case 'admin_template':   return 'COM_FLEXICONTACT_CONFIG_ADMIN_EMAIL_NAME';
        case 'confirm_text':     return 'COM_FLEXICONTACT_CONFIG_CONFIRM_NAME';
        case 'page_text':        return 'COM_FLEXICONTACT_V_TOP_TEXT';
        case 'bottom_text':      return 'COM_FLEXICONTACT_V_BOTTOM_TEXT';
        return $function;
        }
}


}