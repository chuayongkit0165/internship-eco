<?php
/********************************************************************
Product		: Multiple Products
Date		: 4 July 2020
Copyright	: Les Arbres Design 2010-2020
Contact		: http://www.lesarbresdesign.info
Licence		: GNU General Public License
*********************************************************************/
defined('_JEXEC') or die('Restricted Access');

class JFormFieldLadheading extends JFormField
{
protected $type = 'ladheading';

protected function getLabel()
{
    $html = '';
    if (!empty($this->element['title']))
        $html .= '<h3>'.JText::_($this->element['title']).'</h3>';
    if (!empty($this->element['image']))       
        $html .=  '<img src="'.JURI::root(true).$this->element['image'].'" alt="" />';
    if (!empty($this->element['label']))
        $html .= JText::_($this->element['label']);
    return $html;
}

protected function getInput()
{
    $html = '';
    $line_height = '';
    if (!empty($this->element['image']) && (!empty($this->element['desc'])))
        {
        $image_size = getimagesize(JPATH_ROOT.$this->element['image']);
        $image_height = $image_size[1] + 8;
        $line_height = 'line-height:'.$image_height.'px';
        }

    if (!empty($this->element['link']))
        $html .= ' <a href="'.$this->element['link'].'" target="_blank">';

    if (!empty($this->element['desc']))
        $html .= '<span style="'.$line_height.'">'.JText::_($this->element['desc']).'</span>';

    if (!empty($this->element['link']))
        $html .= '</a>';

    return $html;
}


}
