<?php
/********************************************************************
Product		: Multiple Products
Date		: 16 July 2018
Copyright	: Les Arbres Design 2010-2018
Contact		: http://www.lesarbresdesign.info
Licence		: GNU General Public License
*********************************************************************/
defined('_JEXEC') or die('Restricted Access');

JFormHelper::loadFieldClass('text');

class JFormFieldLinkpreview extends JFormFieldText
{
protected $type = 'linkpreview';

protected function getInput()
{    
    $js = "var prevInput = jQuery(this).prev().val();
    if (prevInput == '')
        return false;
    var prefix = prevInput.substring(0, 4);
    if (prefix === 'http')
        var link = prevInput;
    else
        var link = '".JURI::root()."' + prevInput;
    window.open(link,'linkpreview','width=640,height=480,scrollbars=1,location=0,menubar=0,resizable=1');
    return false;";    
    $onclick = 'onclick="'.$js.'"';
    $button = '<button type="button" class="btn btn-info" style="margin-left:5px;"'.$onclick.'>'.JText::_('JGLOBAL_PREVIEW').'</button>';    
	$html = parent::getInput();
    return $html.$button;
}

}