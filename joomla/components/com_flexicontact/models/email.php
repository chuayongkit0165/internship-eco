<?php
/********************************************************************
Product		: Flexicontact
Date		: 31 October 2020
Copyright	: Les Arbres Design 2010-2020
Contact		: http://www.lesarbresdesign.info
Licence		: GNU General Public License
*********************************************************************/
defined('_JEXEC') or die('Restricted access');

class FlexicontactModelEmail extends JModelLegacy
{

//--------------------------------------------------------------------------------
// Initialise the email model data
//
function init_data($config_data)
{
	$this->data = new stdclass();
    
	switch ($config_data->autofill)
		{
		case 'off':
			$this->data->from_name = '';
			$this->data->from_email = '';
			break;
		case 'username':
			$user = JFactory::getUser();
			$this->data->from_name = $user->username;
			$this->data->from_email = $user->email;
			break;
		case 'name':
			$user = JFactory::getUser();
			$this->data->from_name = $user->name;
			$this->data->from_email = $user->email;
			break;
		}
		
	$this->data->subject = $config_data->default_subject;
	$this->data->copy_me = '';
	$this->data->agreement_check = '';
	$this->data->list1 = '';
	$this->data->field1 = '';
	$this->data->field2 = '';
	$this->data->field3 = '';
	$this->data->field4 = '';
	$this->data->field5 = '';
	$this->data->area_data = '';
	$this->data->magic_word = '';
	$this->data->pic_selected = '';
    return $this->data;
}

//--------------------------------------------------------------------------------
// Get post data
//
function getPostData($config_data)
{
    $this->init_data($config_data);
	$jinput = JFactory::getApplication()->input;
	$this->data->from_name = $jinput->get('from_name', $this->data->from_name, 'STRING');
	$this->data->from_email = $jinput->get('from_email', $this->data->from_email, 'STRING');
	$this->data->subject = $jinput->get('subject', $config_data->default_subject, 'STRING');
	$this->data->copy_me = $jinput->get('copy_me', '', 'STRING');						// checkbox
	$this->data->agreement_check = $jinput->get('agreement_check', '', 'STRING');		// checkbox
	$this->data->list1 = $jinput->get('list1', '', 'STRING');
	$this->data->field1 = $jinput->get('field1', '', 'STRING');
	$this->data->field2 = $jinput->get('field2', '', 'STRING');
	$this->data->field3 = $jinput->get('field3', '', 'STRING');
	$this->data->field4 = $jinput->get('field4', '', 'STRING');
	$this->data->field5 = $jinput->get('field5', '', 'STRING');
	$this->data->area_data = $jinput->get('area_data', '', 'STRING');
	$this->data->magic_word = $jinput->get('magic_word', '', 'STRING');
	$this->data->pic_selected = $jinput->get('picselected', '-1', 'STRING');
	return $this->data;
}

// -------------------------------------------------------------------------------
// Validate the user input
// We used to use JSession::checkToken() but that function does an immediate and silent redirect if the token doesn't match
// We want to show a message to the user so we now use a different method
//
function validate(&$errors, $config_data)
{
    $jinput = JFactory::getApplication()->input;
    $token = JSession::getFormToken();
    if (!$jinput->get($token, '', 'string'))			// get token from form
		{
        FC_trace::trace("Session token not in form: $token - token check failed");
		$errors['top'] = JText::_('COM_FLEXICONTACT_SESSION');
		return;
		}
    FC_trace::trace("Session token ok: $token");
    
// if using image captcha, validate that the correct image was chosen
// if the user gets it wrong more than 5 times, tell the controller to kill the session

	if ($config_data->num_images > 0)
		{
		require_once(LAFC_HELPER_PATH.'/flexi_captcha.php');
		$pic_selected = substr($this->data->pic_selected,4);	// strip off the fci_
		$ret = Flexi_captcha::check($pic_selected, $config_data->num_images);
		if ($ret == 1)
			$errors['imageTest'] = JText::_('COM_FLEXICONTACT_WRONG_PICTURE');
		if ($ret == 2)
			{
			$errors['kill'] = 'Yes';		// tell the controller to kill the session
			return;
			}
		}
	
// if using magic word, validate the word

	if ($config_data->magic_word != '')
        {
		if (strcasecmp($this->data->magic_word,$config_data->magic_word) != 0)
			$errors['magic_word'] = JText::_('COM_FLEXICONTACT_WRONG_MAGIC_WORD');
        }
            
// if using the Joomla captcha plugin, call its check function

	if ($config_data->joomla_captcha != 0)
		{
		$plugin = Flexicontact_Utility::get_joomla_captcha();
		if ($plugin)													// if we can't get an instance, we can't test so the result is a PASS
			{
			try
				{
				if ($plugin->checkAnswer('') === false)					// can throw a RuntimeException
					{
					FC_trace::trace("Joomla captcha plugin FAIL");
					$errors['jcaptcha'] = JText::_('COM_FLEXICONTACT_CAPTCHA_WRONG');
					}
				else
					FC_trace::trace("Joomla captcha plugin PASS");
				}
			catch (\RuntimeException $e)
				{
				$jcaptcha_error = $e->getMessage();
				FC_trace::trace("Joomla captcha plugin exception so FAIL ($jcaptcha_error)");
				$errors['bottom'] = JText::_('COM_FLEXICONTACT_CAPTCHA_WRONG');
				}
			}
		}
	
// validate the from name

	if (empty($this->data->from_name))
		$errors['from_name'] = JText::_('COM_FLEXICONTACT_REQUIRED');
    else
        {
        if (strlen($this->data->from_name) < 2)
            $errors['from_name'] = JText::_('COM_FLEXICONTACT_INVALID_NAME');
        if (!preg_match("/^[ ,.'\-\pL]+$/u",$this->data->from_name))           // \pL is the Unicode character class
            $errors['from_name'] = JText::_('COM_FLEXICONTACT_INVALID_NAME');
        if (strlen($this->data->from_name) > 60)
            $this->data->from_name = mb_substr($this->data->from_name, 0, 60);  // truncate to a maximum of 60 characters
        }

// validate the from address
// we no longer accept email addresses without a top level domain name

   	FC_trace::trace("Checking email address: ".$this->data->from_email);	
	if (!JMailHelper::isEmailAddress($this->data->from_email) || !preg_match("/\.\w+$/",$this->data->from_email) )
        {
      	FC_trace::trace(" - Email address is invalid");	
		$errors['from_email'] = JText::_('COM_FLEXICONTACT_BAD_EMAIL');
        }
    else
      	FC_trace::trace(" - Email address is valid");	

// validate the subject

	if (($config_data->show_subject) and (empty($this->data->subject)))
		$errors['subject'] = JText::_('COM_FLEXICONTACT_REQUIRED');

// validate the list selection

	if (($config_data->list_opt == "mandatory") and (empty($this->data->list1)))
		$errors['list'] = JText::_('COM_FLEXICONTACT_SELECT_AN_OPTION');

// validate the user defined text fields

	if (($config_data->field_opt1 == "mandatory") and (empty($this->data->field1)))
		$errors['field1'] = JText::_('COM_FLEXICONTACT_REQUIRED');
	if (($config_data->field_opt2 == "mandatory") and (empty($this->data->field2)))
		$errors['field2'] = JText::_('COM_FLEXICONTACT_REQUIRED');
	if (($config_data->field_opt3 == "mandatory") and (empty($this->data->field3)))
		$errors['field3'] = JText::_('COM_FLEXICONTACT_REQUIRED');
	if (($config_data->field_opt4 == "mandatory") and (empty($this->data->field4)))
		$errors['field4'] = JText::_('COM_FLEXICONTACT_REQUIRED');
	if (($config_data->field_opt5 == "mandatory") and (empty($this->data->field5)))
		$errors['field5'] = JText::_('COM_FLEXICONTACT_REQUIRED');

// validate message

	if (($config_data->area_opt == "mandatory") and (empty($this->data->area_data)))
		$errors['area_data'] = JText::_('COM_FLEXICONTACT_REQUIRED');
}

//-----------------------------------------
// Get client's IP address
//
function getIPaddress()
{
	if (isset($_SERVER["REMOTE_ADDR"]))
		return $_SERVER["REMOTE_ADDR"];
	if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
		return $_SERVER["HTTP_X_FORWARDED_FOR"];
	if (isset($_SERVER["HTTP_CLIENT_IP"]))
		return $_SERVER["HTTP_CLIENT_IP"];
	return "Unknown";
} 

//-------------------------------------------------------------------------------
// Get client's browser
// Returns 99 for unknown, 0 for msie, 1 for Firefox, etc
//
function getBrowser(&$browser_name)
{ 
	if (isset($_SERVER['HTTP_USER_AGENT']))
	    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
	else
		$u_agent = '';

    if (strstr($u_agent, 'Edg')) 
    	{ 
        $browser_name = 'Edge'; 
        return 7; 
    	} 
    if (strstr($u_agent, 'MSIE') && !strstr($u_agent, 'Opera')) 
    	{ 
        $browser_name = 'MSIE'; 
        return 0; 
    	} 
    if (strstr($u_agent, 'Trident')) 
    	{ 
        $browser_name = 'MSIE'; 
        return 0; 
    	} 
    if (strstr($u_agent, 'Firefox')) 
    	{ 
        $browser_name = 'Firefox'; 
        return 1; 
    	} 
    if (strstr($u_agent, 'Chrome')) 	 // must test for Chrome before Safari!
    	{ 
        $browser_name = 'Chrome'; 
        return 3; 
    	} 
    if (strstr($u_agent, 'Safari')) 
    	{ 
        $browser_name = 'Safari'; 
        return 2; 
    	} 
    if (strstr($u_agent, 'Opera')) 
    	{ 
        $browser_name = 'Opera'; 
        return 4; 
    	} 
    $browser_name = 'Unknown Browser';
    return 99;
} 

//-------------------------------------------------------------------------------
// Resolve an email variable
//
function email_resolve($config_data, $variable)
{
    if (!isset($this->data->from_name))     // this field is always mandatory so if we don't have it something is wrong
        {
        if ($variable == LAFC_T_FROM_NAME)
            return '** Form data is missing **';   // should never happen
        return '';
        }
    
	switch ($variable)
		{
		case LAFC_T_FROM_NAME:
			return $this->data->from_name;
		case LAFC_T_FROM_EMAIL:
			return $this->data->from_email;
		case LAFC_T_SUBJECT:
			return $this->data->subject;
		case LAFC_T_MESSAGE_PROMPT:
			return $config_data->area_prompt;
		case LAFC_T_MESSAGE_DATA:
			if ($config_data->email_html)
				$return_body = nl2br($this->data->area_data);
			else
				$return_body = $this->data->area_data;
			return $return_body;
		case LAFC_T_LIST_PROMPT:
			return $config_data->list_prompt;
		case LAFC_T_LIST_DATA:
			return $this->data->list_choice;
		case LAFC_T_FIELD1_PROMPT:
			return $config_data->field_prompt1;
		case LAFC_T_FIELD1_DATA:
			return $this->data->field1;
		case LAFC_T_FIELD2_PROMPT:
			return $config_data->field_prompt2;
		case LAFC_T_FIELD2_DATA:
			return $this->data->field2;
		case LAFC_T_FIELD3_PROMPT:
			return $config_data->field_prompt3;
		case LAFC_T_FIELD3_DATA:
			return $this->data->field3;
		case LAFC_T_FIELD4_PROMPT:
			return $config_data->field_prompt4;
		case LAFC_T_FIELD4_DATA:
			return $this->data->field4;
		case LAFC_T_FIELD5_PROMPT:
			return $config_data->field_prompt5;
		case LAFC_T_FIELD5_DATA:
			return $this->data->field5;
		case LAFC_T_BROWSER:
			return $this->data->browser_string;
		case LAFC_T_IP_ADDRESS:
			return $this->data->ip;
		case LAFC_T_SITE_NAME:
			return JFactory::getApplication()->get('sitename');
		default: return '';
		}
}

//-------------------------------------------------------------------------------
// Merge an email template with post data
//
function email_merge($template_text, $config_data)
{
	$text = $template_text;
	$variable_regex = "#%V_*(.*?)%#s";

	preg_match_all($variable_regex, $text, $variable_matches, PREG_SET_ORDER);

	foreach ($variable_matches as $match)
		{
		$resolved_text = $this->email_resolve($config_data, $match[0]);
		$text = str_replace($match[0], $resolved_text, $text);
		}

	return $text;
}

// -------------------------------------------------------------------------------
// Send the email
// Returns 1 ok, or an error message on failure
//
function sendEmail($config_data)
{
// setup some extra data for the log model to use

	$app = JFactory::getApplication();
	$this->data->ip = $this->getIPaddress();
	$this->data->browser_id = $this->getBrowser($this->data->browser_string);
	$this->data->admin_email = $config_data->toPrimary;
	$this->data->admin_from_email = $app->get('mailfrom');
	$this->data->admin_reply_to_email = $this->data->from_email;        // the email address entered on the contact form
	$this->data->config_show_copy = $config_data->show_copy;
	$this->data->show_copy = $this->data->copy_me;
	$this->data->user_from_email = '';
    
	if ( ($this->data->list1 != '') && (isset($config_data->list_array[$this->data->list1])) )
		$this->data->list_choice = $config_data->list_array[$this->data->list1];
	else
		$this->data->list_choice = '';

    if ($config_data->toPrimary == 'demo@demo.demo')        // demo mode, do not send email
        {
        $this->data->status_main = 'Demo mode, no email sent';
        $this->data->status_copy = '';
        return '1';
        }
    
// build the message to be sent to the site admin

	$merged_admin_body = $this->email_merge($config_data->admin_template, $config_data);    
	$clean_admin_body = JMailHelper::cleanBody($merged_admin_body);
    
    $merged_admin_subject = $this->email_merge($config_data->admin_subject, $config_data);
	$clean_admin_subject = JMailHelper::cleanSubject($merged_admin_subject);

// build the Joomla mail object
// since Joomla 3.5.1 this can throw a phpmailerException

    try
		{
		$mail = JFactory::getMailer();
		if ($config_data->email_html)
			$mail->IsHTML(true);
		else
			$clean_admin_body = strip_tags($clean_admin_body);

		$mail->setSender(array($app->get('mailfrom'), $app->get('fromname')));
		$mail->addRecipient($config_data->toPrimary);
		if (!empty($config_data->ccAddress))
			$mail->addCC($config_data->ccAddress);
		if (!empty($config_data->bccAddress))
			$mail->addBCC($config_data->bccAddress);
		$mail->addReplyTo($this->data->from_email, $this->data->from_name);
		$mail->setSubject($clean_admin_subject);
		$mail->setBody($clean_admin_body);
		if (FC_trace::tracing())
			FC_trace::trace("=====> Sending admin email: ".print_r($mail,true));
		$ret_main = $mail->Send();
		FC_trace::trace("Returned from Joomla Send mail (admin) function");	
		}
	catch (Exception $e)
		{
	    $result_msg = $e->getMessage();
	    $result_code = $e->getCode();
        $this->data->status_main = "Mail Exception: [$result_msg] [$result_code]";
        FC_trace::trace($this->data->status_main);
		return $result_msg;
		}
	
	if ($ret_main === true)
        {
		$this->data->status_main = '1';
		FC_trace::trace("=====> Admin email sent ok");
        }
	else
        {
		$this->data->status_main = $mail->ErrorInfo;
		FC_trace::trace("=====> Admin email send failed: ".$mail->ErrorInfo);
        }
	
// if we should send the user a copy, send it separately

	if (($config_data->show_copy == LAFC_COPYME_ALWAYS) or ($this->data->copy_me == 1))
		{
       	FC_trace::trace("Sending copy email to user");	
    	$this->data->user_from_email = $app->get('mailfrom');       // for the log
		$merged_user_body = $this->email_merge($config_data->user_template, $config_data);
		$clean_user_body = JMailHelper::cleanBody($merged_user_body);
        $merged_user_subject = $this->email_merge($config_data->user_subject, $config_data);
    	$clean_user_subject = JMailHelper::cleanSubject($merged_user_subject);
		$mail = JFactory::getMailer();
		if ($config_data->email_html)
			$mail->IsHTML(true);
		else
			$clean_user_body = strip_tags($clean_user_body);
			
		try
			{
			$mail->setSender(array($app->get('mailfrom'), $app->get('fromname')));
			$mail->addRecipient($this->data->from_email);
			$mail->setSubject($clean_user_subject);
			$mail->setBody($clean_user_body);
			
			if (FC_trace::tracing())
				FC_trace::trace("=====> Sending user email: ".print_r($mail,true));
			$ret_copy = $mail->Send();
	       	FC_trace::trace("Returned from Joomla Send mail (user) function");	
			}
		catch (Exception $e)
			{
			$result_msg = $e->getMessage();
			$result_code = $e->getCode();
			$this->data->status_copy = "Mail Exception: [$result_msg] [$result_code]";
			FC_trace::trace($this->data->status_copy);
			return $result_msg;
			}

		if ($ret_copy === true)
            {
			$this->data->status_copy = '1';
			FC_trace::trace("=====> User email sent ok");
			}
		else
            {
			$this->data->status_copy = $mail->ErrorInfo;
			FC_trace::trace("=====> User email send failed: ".$mail->ErrorInfo);
			}
		}
	else
        {
       	FC_trace::trace("Not sending copy email to user");	
		$this->data->status_copy = '0';		// copy not requested
        }
		
	return $this->data->status_main;		// both statuses are logged, but the main status decides what happens next
}

//-------------------------------------------------------------------------------
// Validate the email addresses configured in the menu item
// - this is called from the front end controller prior to displaying the contact form
// if invalid, returns an error message
// 
function email_check($config_data)
{
	$msg = '';
	
	$lang = JFactory::getLanguage();
	$lang->load(strtolower(LAFC_COMPONENT), JPATH_ADMINISTRATOR.'/components/com_flexicontact');	// load the back end language file

	if (isset($config_data->toPrimary))
		{
		if (empty($config_data->toPrimary) or !JMailHelper::isEmailAddress($config_data->toPrimary))
			$msg .= '('.JText::_('COM_FLEXICONTACT_V_EMAIL_TO').')';
		}
	else
		$msg .= '('.JText::_('COM_FLEXICONTACT_V_EMAIL_TO').')';
		
	if (isset($config_data->ccAddress))
		{
		if (!JMailHelper::isEmailAddress($config_data->ccAddress))
			$msg .= ' ('.JText::_('COM_FLEXICONTACT_V_EMAIL_CC').')';
		}
		
	if (isset($config_data->bccAddress))
		{
		if (!JMailHelper::isEmailAddress($config_data->bccAddress))
			$msg .= ' ('.JText::_('COM_FLEXICONTACT_V_EMAIL_BCC').')';
		}
		
	if ($msg != '')
		$msg = JText::_('COM_FLEXICONTACT_BAD_CONFIG_EMAIL').' - '.$msg;
		
	return $msg;
}

}