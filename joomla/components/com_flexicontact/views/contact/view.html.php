<?php
/********************************************************************
Product		: Flexicontact
Date		: 12 June 2020
Copyright	: Les Arbres Design 2009-2020
Contact		: http://www.lesarbresdesign.info
Licence		: GNU General Public License
*********************************************************************/
defined('_JEXEC') or die('Restricted Access');

class FlexicontactViewContact extends JViewLegacy
{

//---------------------------------------------------------------------------------------------------------
// display the contact form
//
function display($tpl = null)
{
// get the menu item parameters

	$app = JFactory::getApplication('site');
	$document = JFactory::getDocument();
	$menu_params =  $app->getParams();

	if (isset($menu_params->pageclass_sfx))
		echo "\n".'<div class="fc_page'.$menu_params->pageclass_sfx.'">';
	else
		echo "\n".'<div class="fc_page">';

// if there is a page heading in Page Display Options, draw it in H1
	
	if ($menu_params->get('show_page_heading', '0'))						// In menu item Page Display options
		if ($menu_params->get('page_heading', '') != '')
			echo "\n<h1>".$menu_params->get('page_heading', '').'</h1>';

// if there is a page heading in Basic Options, draw it in H2

	if ($menu_params->get('page_hdr', '') != '')							// Basic Options
		echo "\n<h2>".$menu_params->get('page_hdr', '').'</h2>';

// set meta data, if any

	if ($menu_params->get('menu-meta_description'))
		$document->setDescription($menu_params->get('menu-meta_description'));

	if ($menu_params->get('menu-meta_keywords'))
		$document->setMetadata('keywords', $menu_params->get('menu-meta_keywords'));

	if ($menu_params->get('robots'))
		$document->setMetadata('robots', $menu_params->get('robots'));

// add sitename to page title if configured

	if ($app->get('sitename_pagetitles', 0) == 1)
		{
		$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $menu_params->get('page_title', ''));
		$document->setTitle($title);
		}
	if ($app->get('sitename_pagetitles', 0) == 2)
		{
		$title = JText::sprintf('JPAGETITLE', $menu_params->get('page_title', ''), $app->get('sitename'));
		$document->setTitle($title);
		}
		
// draw top text, if any

	if (!empty($this->config_data->page_text))		// top text
		{
		$page_text = JHtml::_('content.prepare', $this->config_data->page_text, '', 'com_flexicontact');
		echo "\n".'<div class="fc_before">'.$page_text.'</div>';
		}

// if the menu item has a width parameter, add it to the fc_outer div as an inline style

    $form_width = $menu_params->get('form_width', '');
    if ($form_width != '')
        $outer_style = ' style="max-width:'.$form_width.';margin-left:auto;margin-right:auto;"';
    else
        $outer_style = '';

	echo '<div class="fc_outer"'.$outer_style.'>';
    
// if we have a 'top' error show it. If not, and if validation failed and the extra message is configured, show that    
    
    if (isset($this->errors['top']))
        echo '<div class="fc_error fc_top_error">'.$this->errors['top'].'</div>';
    elseif ( ($this->config_data->top_error) and !empty($this->errors) )
		echo '<div class="fc_error fc_top_error">'.JText::_('COM_FLEXICONTACT_MESSAGE_NOT_SENT').'</div>';
	
// start the form

    $onsubmit = 'onsubmit="document.getElementById(\'send_button\').disabled=true;"';
	echo "\n".'<form name="fc_form" action="'.JRoute::_('index.php').'" method="post" class="fc_form" '.$onsubmit.'>';
	echo JHTML::_('form.token');
	echo '<input type="hidden" name="option" value="'.LAFC_COMPONENT.'" />';
	echo '<input type="hidden" name="task" value="send" />';

// from name

    if ($this->config_data->autofocus)
        $autofocus = ' autofocus="autofocus"';
    else
        $autofocus = '';

	$class = self::get_class('from_name');
	echo "\n".'<div class="fc_line'.$class.'"><label for="from_name" class="fc_left">'.JText::_('COM_FLEXICONTACT_FROM_NAME').'</label>';
	echo '<input type="text" class="fc_input" name="from_name" id="from_name" value="'.$this->escape($this->post_data->from_name).'"'.$autofocus.' />';
	echo self::get_error('from_name');
	echo '</div>';

// from email address

	$class = self::get_class('from_email');
	echo "\n".'<div class="fc_line'.$class.'"><label for="from_email" class="fc_left">'.JText::_('COM_FLEXICONTACT_FROM_ADDRESS').'</label>';
	echo '<input type="email" class="fc_input" name="from_email" id="from_email" value="'.$this->escape($this->post_data->from_email).'" />';
	echo self::get_error('from_email');
	echo '</div>';

// subject

	if ($this->config_data->show_subject)
		{
		$class = self::get_class('subject');
		echo "\n".'<div class="fc_line'.$class.'"><label for="subject" class="fc_left">'.JText::_('COM_FLEXICONTACT_SUBJECT').'</label>';
		echo '<input type="text" class="fc_input" name="subject" id="subject" value="'.$this->escape($this->post_data->subject).'" />';
		echo self::get_error('subject');
		echo '</div>';
		}

// the select list

	if ($this->config_data->list_opt != 'disabled')
		{
		$class = self::get_class('list');
		$list_html = Flexicontact_Utility::make_list('list1',$this->post_data->list1, $this->config_data->list_array);
		echo "\n".'<div class="fc_line'.$class.'"><label for="list1" class="fc_left">'.$this->config_data->list_prompt.'</label>';
		echo $list_html.self::get_error('list');
		echo '</div>';
		}

// the five optional fields

	for ($i=1; $i<=5; $i++)
		{
		$opt_name = 'field_opt'.$i;
		$prompt_name = 'field_prompt'.$i;
		$field_name = 'field'.$i;
		if ($this->config_data->$opt_name == 'disabled')
			continue;
		if ($this->config_data->$prompt_name == '')
			$this->config_data->$prompt_name = '&nbsp;';
		$class = self::get_class($field_name);
		echo "\n".'<div class="fc_line'.$class.'"><label for="'.$field_name.'" class="fc_left">'.$this->config_data->$prompt_name.'</label>';
		echo '<input type="text" class="fc_input" name="'.$field_name.'" id="'.$field_name.'" value="'.$this->escape($this->post_data->$field_name).'" />';
		echo self::get_error($field_name);
		echo '</div>';
		}

// the main text area

	if ($this->config_data->area_opt != 'disabled')
		{
		if ($this->config_data->area_prompt == '')
			$this->config_data->area_prompt = '&nbsp;';
		$class = self::get_class('area_data');
		echo "\n".'<div class="fc_line fc_msg'.$class.'"><label for="area_data" class="fc_left fc_textarea">'.$this->config_data->area_prompt.'</label>';
		echo '<textarea class="fc_input" name="area_data" id="area_data" rows="'.$this->config_data->area_height.'" >'.$this->escape($this->post_data->area_data).'</textarea>';
		echo self::get_error('area_data');
		echo '</div>';
		}

// the "send me a copy" checkbox
// the empty <span> prevents the extra checkbox added by the Beez template

	if ($this->config_data->show_copy == LAFC_COPYME_CHECKBOX)
		{
		if ($this->post_data->copy_me)
			$checked = 'checked = "checked"';
		else
			$checked = '';
		$checkbox = '<input type="checkbox" class="fc_input" name="copy_me" id="copy_me" value="1" '.$checked.'/>';
		echo "\n".'<div class="fc_line fc_lcb">';
		echo $checkbox.'<span></span><label for="copy_me" class="fc_right">'.JText::_('COM_FLEXICONTACT_COPY_ME').'</label></div>';
		}
	
// the agreement required checkbox

	$send_button_state = '';
	if ($this->config_data->agreement_prompt != '')
		{
		if ($this->post_data->agreement_check)
			$checked = 'checked = "checked"';
		else
			{
			$send_button_state = 'disabled="disabled"';
			$checked = '';
			}
		$onclick = ' onclick="if(this.checked==true){form.send_button.disabled=false;}else{form.send_button.disabled=true;}"';
		$checkbox = '<input type="checkbox" class="fc_input" name="agreement_check" id="agreement_check" value="1" '.$checked.$onclick.'/>';
		$label = self::make_agreement_text($this->config_data->agreement_prompt, $this->config_data->agreement_link);
		echo "\n".'<div class="fc_line fc_lcb">';
		echo $checkbox.'<span></span><label for="agreement_check" class="fc_right">'.' '.$label.'</label></div>';
		}

// the magic word

	if ($this->config_data->magic_word != '')
		{
		$class = self::get_class('magic_word');
		echo "\n".'<div class="fc_line fc_magic'.$class.'"><label for="magic_word" class="fc_left">'.$this->config_data->magic_word_prompt.'</label>';
		echo '<input type="text" class="fc_input" name="magic_word" id="magic_word" value="'.$this->escape($this->post_data->magic_word).'" required="required" />';
		echo self::get_error('magic_word');
		echo '</div>';
		}

// the image captcha

	if ($this->config_data->num_images > 0)
		{
		require_once(LAFC_HELPER_PATH.'/flexi_captcha.php');
		echo Flexi_captcha::show_image_captcha($this->config_data, self::get_error('imageTest'));
		}
        
// include the Joomla captcha plugin, if configured

	if ($this->config_data->joomla_captcha != 0)
		{
		$plugin = Flexicontact_Utility::get_joomla_captcha();
		if ($plugin)
			{
			echo '<div class="fc_line fc_jcap">';
			echo $plugin->display('fcjcap', 'fcjcap', 'fcjcap');
			if (isset($this->errors['jcaptcha']))
				echo self::get_error('jcaptcha');
			echo '</div>';
			}
		}

// the send button

	echo "\n".'<div class="fc_line fc_send">';
	echo '<input type="submit" class="'.$this->config_data->button_class.'" id="send_button" name="send_button" '.$send_button_state.' value="'.JText::_('COM_FLEXICONTACT_SEND_BUTTON').'" />';
	if (isset($this->errors['bottom']))
		echo self::get_error('bottom');
	echo '</div>';
	echo "</form>";
	echo '</div>';					// class="fc_outer"
        
// bottom text

	if (!empty($this->config_data->bottom_text))
		{
		$bottom_text = JHtml::_('content.prepare', $this->config_data->bottom_text, '', 'com_flexicontact');
		echo "\n".'<div class="fc_after">'.$bottom_text.'</div>';
		}
		
	echo "\n</div>";				// class="fc_page"
}

//---------------------------------------------------------------------------------------------------------
// Get the class name for a field outer div
//
function get_class($field_name)
{
	if (isset($this->errors[$field_name]))
		return ' fc_err';
	else
		return '';
}

//---------------------------------------------------------------------------------------------------------
// Get and format an error message
//
function get_error($field_name)
{
	if (isset($this->errors[$field_name]))
		return '<span class="fc_error">'.$this->errors[$field_name].'</span>';
	else
		return '';
}

//-------------------------------------------------------------------------------
// Make an HTML string that includes a popup link.
// $label contains %link text%, like this: "Please read our %privacy policy% and confirm your agreement"
// $link is a URL
//
static function make_agreement_text($label, $link)
{
	$matches = array();
	$result = preg_match('/.*?%(.*?)%/is',$label,$matches);
	if ($result === false)
		return $label;			// the link is optional
	$link_name = $matches[1];	// the text inside the %...%

	$link = trim($link);
	if (empty($link))
		return $label;			// no link specified

	$popup = 'onclick="window.open('."'".$link."', 'fcagreement', 'width=640,height=480,scrollbars=1,location=0,menubar=0,resizable=1'); return false;".'"';
	$link_html = JHTML::link($link, $link_name, 'target="_blank" '.$popup);
	$label = str_replace('%'.$link_name.'%', $link_html, $label);
	return $label;
}

}