(function ($) {
    "use strict";

    var easyContactsCaptchas = {};

    var initCaptcha = function () {
        $('.djeasycontact-g-recaptcha').each(function () {
            var item = $(this);


            easyContactsCaptchas[item.attr('id')] =
                grecaptcha.render(item.get(0), {
                    sitekey: item.attr('data-sitekey'),
                    theme: item.attr('data-theme'),
                    size: item.attr('data-size'),
                    callback: item.attr('data-callback')
                });
        });
    };

    window.DJEasyContactInitCaptcha = initCaptcha;

    $.fn.DJEasyContact = function (config) {

        var options = {
            style: 1,
            id: 0,
            recaptcha: 0
        };
        Object.assign(options, config);


        var _self = this;

        _self.options = options;
        _self.container = _self.closest('.dj-easy-contact-wrapper');
        _self.msgContainer = _self.container.find('.dj-easy-contact-message');
        _self.form = _self.container.find('form');

        var toastrIsEnabled = (typeof DJNotificationsOptions !== 'undefined');

        var checkInputs = function (items) {
            items.each(function () {

                if ($(this).val() === '') {
                    $(this).removeClass('not-empty');
                } else {
                    $(this).addClass('not-empty');
                }
            });
        }

        // check if inputs are empty
        var inputs = _self.container.find('input[type="text"], input[type="email"], textarea');
        checkInputs(inputs);

        inputs.change(function () {
            checkInputs($(this));
        });

        // keyboard nav - checkbox
        var checkbox = _self.container.find("input[type='checkbox']");
        checkbox.keydown(function (event) {
            if (event.which === 13) { // Enter key
                event.preventDefault();
                if ($(this).is(':checked')) {
                    $(this).prop("checked", false);
                } else {
                    $(this).prop("checked", true);
                }
            }
        });

        var showMessage = function (message, style) {
            _self.msgContainer.html(); //clear messages



            if (typeof message != 'undefined' || message) {
                if (typeof style == 'undefined') {
                    style = 'warning';
                }



                if(toastrIsEnabled) {
                    $(document).trigger('djtoastr:renderMessage',[message, style]);
                }else {
                    var m = $("<div class=\'alert alert-easy-contact alert-" + style + "\'><button type=\'button\' data-dismiss=\'alert\' class=\'close\'>×</button><div> " + message + "</div></div>");
                    _self.msgContainer.append(m);
                }
            }
        };

        $(this).on('submit', function (e) {
            e.preventDefault();

            var formData = $(this).serialize();
            var url = $(this).attr('action');


            $.ajax({
                type: "POST",
                url: url,
                data: formData
            }).done(function (response) {
                if (response === "OK") {


                    if(typeof grecaptcha !== 'undefined') {
                        grecaptcha.reset();
                    }


                    if (_self.options.redirect !== '') {
                        location.href = options.redirect;
                    } else {
                        if (_self.options.style === 5) {
                            _self.form.hide();

                            _self.container.find("#modal-dj-easy-contact-box .close, .modal-backdrop").click(function () {
                                _self.msgContainer.empty();
                                _self.form.show();
                            });
                        }
                        showMessage(Joomla.JText._('MOD_DJ_EASYCONTACT_AJAX_OK'), 'success');
                        _self.form.trigger("reset");
                        return false; // Prevent page refresh
                    }
                } else if (response === "CAPTCHA_ERROR") {
                    showMessage(Joomla.JText._('MOD_DJ_EASYCONTACT_WRONG_CAPTCHA'), 'error')
                } else {
                    showMessage(Joomla.JText._('MOD_DJ_EASYCONTACT_AJAX_FAIL'), 'error');
                }
            }).fail(function (xhr, status, error) {
                showMessage(Joomla.JText._('MOD_DJ_EASYCONTACT_AJAX_FAIL'), 'error');
            });

        });

        $(document).on('click', '#dj-easy-contact-send-' + _self.options.id, function (e) {
            e.preventDefault();
            var isValid = document.formvalidator.isValid(_self.form);

            // clear messages
            _self.msgContainer.empty();

            if (isValid) {

                if (_self.options.recaptcha === 2) {

                    var grecaptchaId = jQuery(_self.form.find('.djeasycontact-g-recaptcha')[0]).attr('id');
                    var item = ((typeof easyContactsCaptchas[grecaptchaId] !== 'undefined') ? easyContactsCaptchas[grecaptchaId] : null);
                    grecaptcha.execute(item);
                } else {
                    if(grecaptcha.getResponse() == "") {
                        e.preventDefault();
                        showMessage(Joomla.JText._('MOD_DJ_EASYCONTACT_WRONG_CAPTCHA'), 'error')
                    } else {
                        $(_self).trigger('submit');
                    }
                }
            } else if(!toastrIsEnabled) {
                // move system message alerts to easy-contact message container
                $('#system-message-container .alert').last().appendTo(_self.msgContainer);
            }

            return;
        });

    };

})(jQuery);
