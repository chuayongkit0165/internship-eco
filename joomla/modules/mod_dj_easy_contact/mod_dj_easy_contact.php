<?php

/**
 * @version $Id: mod_dj_easy_contact.php 20 2015-02-06 15:57:45Z marcin $
 * @package DJ-EasyContact
 * @copyright Copyright (C) 2012 DJ-Extensions.com, All rights reserved.
 * @license http://www.gnu.org/licenses GNU/GPL
 * @author url: http://dj-extensions.com
 * @author email contact@dj-extensions.com
 * @developer Marcin Łyczko - marcin.lyczko@design-joomla.eu
 *
 *
 * DJ-EasyContact is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DJ-EasyContact is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DJ-EasyContact. If not, see <http://www.gnu.org/licenses/>.
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
JHtml::_('jquery.framework');
// get Joomla version
$version = new JVersion;
$jversion = '3';
if (version_compare($version->getShortVersion(), '3.0.0', '<')) {
	$jversion = '2.5';
}

// get module ID
$moduleId = $module->id;

// get style
$style = $params->get('styles', '');
if ($style == 0) {
	$style_file = '1';
} else if ($style == 1) {
	$style_file = '2';
} else if ($style == 2) {
	$style_file = '3';
} else if ($style == 3) {
	$style_file = '4';
} else if ($style == 4) {
	$style_file = '5';
}

JText::script ('MOD_DJ_EASYCONTACT_AJAX_OK');
JText::script ('MOD_DJ_EASYCONTACT_AJAX_FAIL');
JText::script ('MOD_DJ_EASYCONTACT_WRONG_CAPTCHA');

$doc = JFactory::getDocument();
// load stylesheet
$doc->addStylesheet(JUri::base() . 'modules/mod_dj_easy_contact/assets/mod_dj_easy_contact.css');
$doc->addStylesheet(JUri::base() . 'modules/mod_dj_easy_contact/assets/style' . $style_file . '.css');

// add scripts
$jquery = $params->get('jquery', '');
if ($jquery == 1) {
	if ($jversion == '3') {
		JHtml::_('jquery.framework', true);
	} else {
		$doc->addScript('//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js');
	}
}

//Get language code
$lang = JFactory::getLanguage();
$lang_tag = $lang->getTag();
$lang_code_array = explode("-", $lang_tag);
$lang_code =  $lang_code_array[0];

//Global
$form_fields = $params->get('fields_labels', 1);
$introtext = $params->get('introtext', '');


//Form actions settings
$recipient = $params->get('email_recipient', '');
$fromEmail = @$params->get('from_email', '');
$sendersname = @$params->get('from_email_name', '');
$more_details = $params->get('more_details', true);
$mySubject = $params->get('email_subject', '');
$redirectURLSwitch = $params->get('redirect_url_switch', false);
$redirectURL = $params->get('redirect_url', '');


// Captcha Parameters
$enable_anti_spam = $params->get('enable_anti_spam', 1);
$recaptcha_site_key = $params->get('recaptcha_site_key', '');
$recaptcha_secret_key = $params->get('recaptcha_secret_key', '');
$invisible_captcha_badge = $params->get('invisible_captcha_badge', '');
if ($invisible_captcha_badge == 0) {
	$invisible_captcha_badge_class = 'bottomleft';
} else if ($invisible_captcha_badge == 1) {
	$invisible_captcha_badge_class = 'bottomright';
} else if ($invisible_captcha_badge == 2) {
	$invisible_captcha_badge_class = 'inline';
} else if ($invisible_captcha_badge == 3) {
	$invisible_captcha_badge_class = 'csshidden';
}

// thanks options
$email_thanks = $params->get('email_thanks', true);
$email_thanks_subject = $params->get('thanks_subject', '');
$email_thanks_message = $params->get('email_thanks_message', '');

// Module Class Suffix Parameter
$mod_class_suffix = $params->get('moduleclass_sfx', '');

$error_message = '';
$valid_name = '';
$valid_email = '';
$valid_message = '';

$doc->addScript(JUri::base() . '/modules/mod_dj_easy_contact/assets/script.js');

if ($enable_anti_spam == 1 || $enable_anti_spam == 2) {
	$headerdata = $doc->getHeadData();
	$scripts = $headerdata['scripts'];
	$headerdata['scripts'] = array();

	$plgPath = preg_quote('media/plg_captcha_recaptcha/js/', '/');
	$apiPath = preg_quote('https://www.google.com/recaptcha/api.js', '/');
	foreach ($scripts as $url => $type) {
		if (
			preg_match('#' . $plgPath . '#s', $url) == false
			&& preg_match('#' . $apiPath . '#s', $url) == false
		) {
			$headerdata['scripts'][$url] = $type;
		}
	}
	$doc->setHeadData($headerdata);
	//&amp;render=explicit
	JHtml::script('https://www.google.com/recaptcha/api.js?hl=' . $lang_code . '&amp;onload=DJEasyContactInitCaptcha&amp;render=explicit');
}

require JModuleHelper::getLayoutPath('mod_dj_easy_contact', $params->get('layout', 'default'));
