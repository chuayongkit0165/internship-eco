<?php
/**
 * @version $Id: mod_dj_easy_contact.php 20 2015-02-06 15:57:45Z marcin $
 * @package DJ-EasyContact
 * @copyright Copyright (C) 2012 DJ-Extensions.com, All rights reserved.
 * @license http://www.gnu.org/licenses GNU/GPL
 * @author url: http://dj-extensions.com
 * @author email contact@dj-extensions.com
 * @developer Marcin Łyczko - marcin.lyczko@design-joomla.eu
 *
 *
 * DJ-EasyContact is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DJ-EasyContact is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DJ-EasyContact. If not, see <http://www.gnu.org/licenses/>.
 *
 */

use Joomla\Registry\Registry;
class modDJEasyContactHelper {

    public static function sendMessageAjax() {
        $app = JFactory::getApplication();
        $input = $app->input;

        $moduleId = $input->getInt('id');
        $db= JFactory::getDbo();
        $db->setQuery('SELECT * FROM #__modules WHERE id = ' . (int) $moduleId);
        $module = $db->loadObject();
        $params = new Registry($module->params);

        $dj_name = $input->post->getString('dj_name');
        $valid_name = htmlentities($dj_name, ENT_COMPAT, "UTF-8");
        $dj_message = $input->post->getString('dj_message');
        if ($dj_message) {
            $valid_message = htmlentities($dj_message, ENT_COMPAT, "UTF-8");
        }

        $dj_email = $input->post->getString('dj_email');
        if ($dj_email) {
            $valid_email = htmlentities($dj_email, ENT_COMPAT, "UTF-8");
        }
//Global
        $form_fields = $params->get('fields_labels', 1);
        $required_fields_appearance = $params->get('required_fields_appearance', 1);
        $introtext = $params->get('introtext', '');

//Name Parameters
        $name_enabled = $params->get('name_enabled', true);
        $name_required = $params->get('name_required', true);
        $name_label = $params->get('name_label', '');
        $name_placeholder = $params->get('name_placeholder', '');

//Email Parameters
        $email_enabled = $params->get('email_enabled', true);
        $email_required = $params->get('email_required', true);
        $email_label = $params->get('email_label', '');
        $email_placeholder = $params->get('email_placeholder', '');

// Message Parameters
        $message_enabled = $params->get('message_enabled', true);
        $message_required = $params->get('message_required', true);
        $message_type = $params->get('message_type', true);
        $message_label = $params->get('message_label_real', true);
        $message_placeholder = $params->get('message_label', true);

        // thanks options
        $email_thanks = $params->get('email_thanks', true);
        $email_thanks_subject = $params->get('thanks_subject', '');
        $email_thanks_message = $params->get('email_thanks_message', '');


//Form actions settings
        $recipient = $params->get('email_recipient', '');
        $fromEmail = @$params->get('from_email', '');
        $sendersname = @$params->get('from_email_name', '');
        $more_details = $params->get('more_details', true);
        $mySubject = $params->get('email_subject', '');
        $redirectURLSwitch = $params->get('redirect_url_switch', false);
        $redirectURL = $params->get('redirect_url', '');
        $redirectJS = "";
        if (($redirectURLSwitch == true) && ($redirectURL != "")) {
            $redirectJS = 'jQuery(location).attr("href", "' . $redirectURL . '");';
        }

// Consent parameters
        $rodo_enabled = $params->get('rodo_enabled', false);
        $rodo_text = $params->get('rodo_text', false);
        $rodo_enabled2 = $params->get('rodo_enabled2', false);
        $rodo_text2 = $params->get('rodo_text2', false);

// Captcha Parameters
        $enable_anti_spam = $params->get('enable_anti_spam', true);
        $recaptcha_site_key = $params->get('recaptcha_site_key', '');
        $recaptcha_secret_key = $params->get('recaptcha_secret_key', '');
        $invisible_captcha_badge = $params->get('invisible_captcha_badge', '');


        if ($enable_anti_spam == 1 || $enable_anti_spam == 2) {
            $g_recaptcha_response = $input->post->getString('g-recaptcha-response');
            if ($g_recaptcha_response) {
                $captcha = $g_recaptcha_response;
            }
            /*$contextOpts = array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false
                )
            );*/
            //$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret='".$recaptcha_secret_key."'&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR'], false, stream_context_create($contextOpts));

            // CURL Check
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
            $captchaPost = array(
                'secret' => $recaptcha_secret_key,
                'response' => $captcha,
                'remote_id' => $_SERVER['REMOTE_ADDR']
            );
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $captchaPost);
            $response = curl_exec($ch);

            if ($response === false) {
                header("Content-type: text/html; charset=utf-8");
                if (!count(array_diff(ob_list_handlers(), array('default output handler'))) || ob_get_length() || ob_get_level() > 0) {
                    while (@ob_end_clean());
                }
                echo JText::_('MOD_DJ_EASYCONTACT_WRONG_CAPTCHA');
                die();
            }
            // CURL Check end

            $result_captcha = json_decode($response, true);
            if ($result_captcha['success'] != 1) {
                header("Content-type: text/html; charset=utf-8");
                if (!count(array_diff(ob_list_handlers(), array('default output handler'))) || ob_get_length() || ob_get_level() > 0) {
                    while (@ob_end_clean());
                }
                echo JText::_('MOD_DJ_EASYCONTACT_WRONG_CAPTCHA');
                die();
                $error_message .=  ' <span class="error-dj-simple-contact-form">' . JText::_('MOD_DJ_EASYCONTACT_WRONG_CAPTCHA') . '</span>';
            }
        }

        // get useful info
        $current_url = JText::_('MOD_DJ_EASYCONTACT_SEND_FROM') . ' ' . JURI::current();
        $user_ip = JText::_('MOD_DJ_EASYCONTACT_IP') . ' ' . $_SERVER['REMOTE_ADDR'];
        jimport('joomla.environment.browser');
        $doc = JFactory::getDocument();
        $browser = JBrowser::getInstance();
        $browserType = JText::_('MOD_DJ_EASYCONTACT_BROWSER_TYPE') . ' ' . $browser->getBrowser();
        $browserVersion = JText::_('MOD_DJ_EASYCONTACT_BROWSER_VERSION') . ' ' . $browser->getMajor();
        $full_agent_string = JText::_('MOD_DJ_EASYCONTACT_FULL_AGENT_STRING') . ' ' . $browser->getAgentString();
        $agreement = '';
        $agreement2 = '';
        if ($rodo_enabled) {
            $agreement = JText::_('MOD_DJ_EASYCONTACT_AGREEMENT') . ' ' . strip_tags($rodo_text);
        }
        if ($rodo_enabled2) {
            $agreement2 = JText::_('MOD_DJ_EASYCONTACT_AGREEMENT2') . ' ' . strip_tags($rodo_text2);
        }

        if ($email_enabled == 1) {
            $message_text = JText::_('MOD_DJ_EASYCONTACT_MESSAGE_INFO') . ' ' . $dj_name . ' - ' . $dj_email . "\n\n" . $mySubject . "\n\n" . $dj_message . "\n\n" . $current_url . "\n\n" . $user_ip . "\n\n";

            if ($more_details) {
                $message_text .= $browserType . "\n\n" . $browserVersion . "\n\n" . $full_agent_string . "\n\n";
            }

            $message_text .= $agreement . "\n\n" . $agreement2;
        } else {
            $message_text = $dj_message . "\n\n" . $current_url . "\n\n" . $user_ip . "\n\n";

            if ($more_details) {
                $message_text .= $browserType . "\n\n" . $browserVersion . "\n\n" . $full_agent_string . "\n\n";
            }

            $message_text .= $agreement . "\n\n" . $agreement2;
        }

        // sending email to admin
        $mailSender = JFactory::getMailer();
        $mailSender->setSender(array($fromEmail, $dj_name));
        $mailSender->addRecipient($recipient);
        if ($email_enabled) {
            $mailSender->addReplyTo($dj_email, '');
        }
        $mailSender->setSubject($mySubject);
        $mailSender->setBody($message_text);
        $mailSender->send();

        // sending thanks message
        if ($email_thanks && $email_enabled) {
            $agreement_user = "";
            $agreement_user2 = "";
            if ($rodo_enabled) {
                $agreement_user = "\n\n" . JText::_('MOD_DJ_EASYCONTACT_AGREEMENT') . "\n" . strip_tags($rodo_text);
            }
            if ($rodo_enabled2) {
                $agreement_user2 = "\n\n" . JText::_('MOD_DJ_EASYCONTACT_AGREEMENT2') . "\n" . strip_tags($rodo_text2);
            }

            $mailSender_thanks = JFactory::getMailer();
            $mailSender_thanks->addRecipient($dj_email);

            $mailSender_thanks->setSender(array($fromEmail, $sendersname));
            $mailSender_thanks->addReplyTo($fromEmail, '');

            $mailSender_thanks->setSubject($email_thanks_subject);
            $mailSender_thanks->setBody($email_thanks_message . "\n\n" . JText::_('MOD_DJ_EASYCONTACT_YOUR_MESSAGE') . "\n" . $dj_message . $agreement_user . $agreement_user2);

            $mailSender_thanks->Send();
        }
        if (!count(array_diff(ob_list_handlers(), array('default output handler'))) || ob_get_length() || ob_get_level() > 0) {
            while (@ob_end_clean());
        }
        echo 'OK';
        jexit();
    }

    public static function validateCaptcha() {

    }

}
