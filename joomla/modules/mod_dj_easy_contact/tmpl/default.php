<?php

/**
 * @version $Id: mod_dj_easy_contact.php 20 2015-02-06 15:57:45Z marcin $
 * @package DJ-EasyContact
 * @copyright Copyright (C) 2012 DJ-Extensions.com, All rights reserved.
 * @license http://www.gnu.org/licenses GNU/GPL
 * @author url: http://dj-extensions.com
 * @author email contact@dj-extensions.com
 * @developer Marcin Łyczko - marcin.lyczko@design-joomla.eu
 *
 *
 * DJ-EasyContact is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DJ-EasyContact is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DJ-EasyContact. If not, see <http://www.gnu.org/licenses/>.
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
// getting form fields
$input = JFactory::getApplication()->input;

// get Joomla version
$version = new JVersion;
$jversion = '3';
if (version_compare($version->getShortVersion(), '3.0.0', '<')) {
    $jversion = '2.5';
}

$redirectURLSwitch = $params->get('redirect_url_switch', false);
$redirectURL = (($redirectURLSwitch && $params->get('redirect_url', '')) ? $params->get('redirect_url', '') : '') ;

// class for captcha
//$enable_anti_spam = $params->get('enable_anti_spam', 1);
if ($enable_anti_spam == 0) {
    $antispam_class = "disabled-captcha";
} else if ($enable_anti_spam == 1) {
    $antispam_class = "no-captcha";
} else if ($enable_anti_spam == 2) {
    $antispam_class = "invisible-captcha";
}

// add library for form validation
JHTML::_('behavior.formvalidation');

// message for Joomla 2.5 when style 5 is selected
if ($style_file == '5') {
    if ($jversion == '2.5') {
        echo JText::_('MOD_DJ_EASYCONTACT_JOOMLA_OLD_MESSAGE');
        return;
    }
    JHTML::_('behavior.modal');
}

// check recipient
if ($recipient === "") {
    $myReplacement = '<span class="error-dj-simple-contact-form">' . JText::_('MOD_DJ_EASYCONTACT_NO_RECIPIENT') . '</span>';
    print $myReplacement;
    return true;
}

// add class special class if email field is active or not
$email_enabled = $params->get('email_enabled', true);
$is_email_field_on = ( $email_enabled != 1 ) ? 'email-field-not-active' : 'email-field-active';
?>

<div id="dj-easy-contact-<?php echo $moduleId; ?>" class="dj-easy-contact-wrapper <?php echo $mod_class_suffix; ?>">
	<?php if ($style_file == '5') : ?>
		<a href="#modal-dj-easy-contact-box" role="button" class="btn dj-easy-contact-modal-button" data-toggle="modal"><span><?php echo JText::_('MOD_DJ_EASYCONTACT_OPEN_FORM'); ?></span></a>
		<div id="modal-dj-easy-contact-box" class="modal hide fade">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<span><?php echo JText::_('MOD_DJ_EASYCONTACT_CLOSE'); ?></span></button>
				<h3 id="myModalLabel"><?php echo $module->title; ?></h3>
			</div>
			<div class="modal-body">
			
	<?php endif; ?>

			<div class="dj-simple-contact-form form-labels-<?php echo $form_fields; ?> style-<?php echo $style_file . ' ' . $is_email_field_on . ' badge-' . $invisible_captcha_badge_class; ?>">
				<div class="dj-easy-contact-message"></div>
				<form class="easy-contact-form form-validate <?php echo $antispam_class; ?>"
					id="dj-easy-contact-form-<?php echo $moduleId; ?>"
					action="<?php echo JRoute::_('index.php?option=com_ajax&module=dj_easy_contact&method=sendMessage&format=json&id=' . (int)$module->id) ?>"
					method="post">

					<span class="dj-simple-contact-form-introtext"><?php echo $introtext; ?></span>
					<?php if ($error_message) : ?>
						<?php print $error_message; ?>
					<?php endif; ?>

					<?php require JModuleHelper::getLayoutPath('mod_dj_easy_contact', 'fields'); ?>
				</form>
			</div>
			<?php if ($style_file == '5') : ?>
		</div>
	</div>
	<?php endif; ?>
</div>

<script>
	function DJEasyContactSubmit<?php echo $module->id;?>( token ) {
		console.log('submit <?php echo $module->id;?>');
        jQuery('#dj-easy-contact-form-<?php echo $moduleId; ?>').trigger('submit');
    };
    jQuery(document).ready(function () {
        jQuery('#dj-easy-contact-form-<?php echo $moduleId; ?>').DJEasyContact({
            id: <?php echo $moduleId; ?>,
            style:<?php echo $style_file; ?>,
            redirect: '<?php echo $redirectURL; ?>',
            recaptcha: <?php echo $enable_anti_spam; ?>
		});
    });
</script>
