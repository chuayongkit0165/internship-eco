<?php

/**
 * @version $Id: mod_dj_easy_contact.php 20 2015-02-06 15:57:45Z marcin $
 * @package DJ-EasyContact
 * @copyright Copyright (C) 2012 DJ-Extensions.com, All rights reserved.
 * @license http://www.gnu.org/licenses GNU/GPL
 * @author url: http://dj-extensions.com
 * @author email contact@dj-extensions.com
 * @developer Marcin Łyczko - marcin.lyczko@design-joomla.eu
 *
 *
 * DJ-EasyContact is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DJ-EasyContact is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DJ-EasyContact. If not, see <http://www.gnu.org/licenses/>.
 *
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$required_fields_appearance = $params->get('required_fields_appearance', 2);

//Name Parameters
$name_enabled = $params->get('name_enabled', true);
$name_required = $params->get('name_required', true);
$name_label = $params->get('name_label', '');
$name_placeholder = $params->get('name_placeholder', '');

//Email Parameters
$email_enabled = $params->get('email_enabled', true);
$email_required = $params->get('email_required', true);
$email_label = $params->get('email_label', '');
$email_placeholder = $params->get('email_placeholder', '');

// Message Parameters
$message_enabled = $params->get('message_enabled', true);
$message_type = $params->get('message_type', true);
$message_required = $params->get('message_required', true);
$message_label = $params->get('message_label_real', '');
$message_placeholder = $params->get('message_label', '');

// Consent parameters
$rodo_enabled = $params->get('rodo_enabled', false);
$rodo_text = $params->get('rodo_text', '');
$rodo_enabled2 = $params->get('rodo_enabled2', false);
$rodo_text2 = $params->get('rodo_text2', '');

// required fields
$name_required_text = '';
$email_required_text = '';
$message_required_text = '';

$name_required_placeholder_text = '';
$email_required_placeholder_text = '';
$message_required_placeholder_text = '';

$name_required_attribute = '';
$email_required_attribute = '';
$message_required_attribute = '';

$placeholder_name = '';
$placeholder_email = '';
$placeholder_message = '';

$star = ( $required_fields_appearance ) ? ' <span class="star">*</span>' : '';
$required_label = ( $required_fields_appearance ) ? ' <span class="req">' . JText::_('MOD_DJ_EASYCONTACT_REQUIRED') . '</span>' : '';

if ($name_required) {
	$name_required_text = ( $required_fields_appearance == 1 ) ? $star . $required_label : $star;
	$name_required_attribute = 'required="required"';
	$name_required_placeholder_text = ($form_fields == 1) ? strip_tags($name_required_text) : '';
}
if ($email_required) {
	$email_required_text = ( $required_fields_appearance == 1 ) ? $star . $required_label : $star;
	$email_required_attribute = 'required="required"';
	$email_required_placeholder_text = ($form_fields == 1) ? strip_tags($email_required_text) : '';
}
if ($message_required) {
	$message_required_text = ( $required_fields_appearance == 1 ) ? $star . $required_label : $star;
	$message_required_attribute = 'required="required"';
	$message_required_placeholder_text = ($form_fields == 1) ? strip_tags($message_required_text) : '';
}

$required_consent = ( $required_fields_appearance == 1 ) ? $star . $required_label : $star;

// get labels
$name_label_text = ($name_label == '') ? JText::_('MOD_DJ_EASYCONTACT_NAME_LABEL_REAL') : $name_label;
$name_placeholder_text = ($name_placeholder == '') ? JText::_('MOD_DJ_EASYCONTACT_NAME_PLACEHOLDER_LABEL') : $name_placeholder;

$email_label_text = ($email_label == '') ? JText::_('MOD_DJ_EASYCONTACT_EMAIL_LABEL_REAL') : $email_label;
$email_placeholder_text = ($email_placeholder == '') ? JText::_('MOD_DJ_EASYCONTACT_EMAIL_PLACEHOLDER_LABEL') : $email_placeholder;

$message_label_text = ($message_label == '') ? JText::_('MOD_DJ_EASYCONTACT_MESSAGE_LABEL_REAL') : $message_label;
$message_placeholder_text = ($message_placeholder == '') ? JText::_('MOD_DJ_EASYCONTACT_MESSAGE_PLACEHOLDER_LABEL') : $message_placeholder;


// labels or placeholders
if ( (($form_fields == '1' || $form_fields == '3') && $style_file != '5') ) {
	$placeholder_name = "placeholder='" . $name_placeholder_text . $name_required_placeholder_text . "'";
	$placeholder_email = "placeholder='" . $email_placeholder_text . $email_required_placeholder_text . "'";
	$placeholder_message = "placeholder='" . $message_placeholder_text . $message_required_placeholder_text . "'";
}

?>

<div class="dj-simple-contact-form">
	<?php if ($required_fields_appearance == 2) : ?>
	<p class="dj-simple-contact-form-required-fields-info"><?php echo JText::_('MOD_DJ_EASYCONTACT_REQUIRED_FIELDS_TOP'); ?></p>
	<?php endif; ?>

	<?php if ($name_enabled) : ?>
	<div class="dj-simple-contact-form-row name">

		<input aria-labelledby="djec-name" <?php echo $placeholder_name; ?>
			class="dj-simple-contact-form djec-name inputbox <?php echo $mod_class_suffix; ?>" type="text"
			name="dj_name" id="dj_name-<?php echo $moduleId; ?>" value="<?php echo $valid_name; ?>"
			<?php echo $name_required_attribute; ?> autocomplete="name" />

		<?php if ($style_file == '5') : ?>
		<span class="highlight"></span>
		<span class="bar"></span>
		<?php endif; ?>

		<label id="djec-name"
			for="dj_name-<?php echo $moduleId; ?>"><?php echo $name_label_text . $name_required_text; ?></label>

	</div>
	<?php endif; ?>

	<?php if ($email_enabled) : ?>
	<div class="dj-simple-contact-form-row email">

		<input aria-labelledby="djec-email" <?php echo $placeholder_email; ?>
			class="dj-simple-contact-form inputbox validate-email <?php echo $mod_class_suffix; ?>" type="email"
			name="dj_email" id="dj_email-<?php echo $moduleId; ?>" value="<?php echo $valid_email; ?>"
			<?php echo $email_required_attribute; ?> autocomplete="email" />

		<?php if ($style_file == '5') { ?>
		<span class="highlight"></span>
		<span class="bar"></span>
		<?php } ?>

		<label id="djec-email"
			for="dj_email-<?php echo $moduleId; ?>"><?php echo $email_label_text . $email_required_text; ?></label>

	</div>
	<?php endif; ?>

	<?php if ($message_enabled) : ?>
		<?php if ($message_type == 1) : ?>
		<div class="dj-simple-contact-form-row message">

			<textarea aria-labelledby="djec-message" <?php echo $placeholder_message; ?>
				class="dj-simple-contact-form textarea <?php echo $mod_class_suffix; ?>" name="dj_message"
				id="dj_message-<?php echo $moduleId; ?>" cols="4" rows="4" <?php echo $message_required_attribute; ?>><?php echo $valid_message; ?></textarea>

			<?php if ($style_file == '5') { ?>
			<span class="highlight"></span>
			<span class="bar"></span>
			<?php } ?>

			<label id="djec-message"
				for="dj_message-<?php echo $moduleId; ?>"><?php echo $message_label_text . $message_required_text; ?></label>

		</div>
		<?php else : ?>
		<div class="dj-simple-contact-form-row message">

			<input aria-labelledby="djec-message" <?php echo $placeholder_message; ?>
				class="dj-simple-contact-form inputbox <?php echo $mod_class_suffix; ?>" type="text" name="dj_message"
				id="dj_message-<?php echo $moduleId; ?>" value="<?php echo $valid_message; ?>"
				<?php echo $message_required_attribute; ?> />

			<?php if ($style_file == '5') : ?>
			<span class="highlight"></span>
			<span class="bar"></span>
			<?php endif; ?>

			<label id="djec-message"
				for="dj_message-<?php echo $moduleId; ?>"><?php echo $message_label_text . $message_required_text; ?></label>

		</div>
		<?php endif; ?>
	<?php endif; ?>

	<fieldset class="dj-simple-contact-form-row terms-conditions checkboxes" id="terms_and_conditions">
		<legend class="sr-only"><?php echo JText::_('MOD_DJ_EASYCONTACT_FIELDSET1_LEGEND'); ?></legend>
		<?php if ($rodo_enabled) : ?>
		<input type="checkbox" name="dj_easy_contact_terms_and_conditions_input"
			id="dj_easy_contact_terms_and_conditions_input-<?php echo $moduleId; ?>" value="0" required="required">
		<label class="label_terms" for="dj_easy_contact_terms_and_conditions_input-<?php echo $moduleId; ?>"
			id="terms_and_conditions-lbl" aria-required="true">
			<?php echo $rodo_text . $required_consent; ?>
		</label>
		<?php endif; ?>

		<?php if ($rodo_enabled2) : ?>
		<input type="checkbox" name="dj_easy_contact_terms_and_conditions_input2"
			id="dj_easy_contact_terms_and_conditions_input2-<?php echo $moduleId; ?>" value="0" required="required">
		<label class="label_terms" for="dj_easy_contact_terms_and_conditions_input2-<?php echo $moduleId; ?>"
			id="terms_and_conditions2-lbl" aria-required="true">
			<?php echo $rodo_text2 . $required_consent; ?>
		</label>
		<?php endif; ?>
	</fieldset>

	<?php if (($enable_anti_spam == 1 || $enable_anti_spam == 2) && $recaptcha_site_key && $recaptcha_secret_key) : ?>
		<?php if ($enable_anti_spam == 1) : ?>
		<div class="djeasycontact-g-recaptcha" id="djeasycontact-g-recaptcha-<?php echo $moduleId; ?>"
			data-sitekey="<?php echo $params->get('recaptcha_site_key', ''); ?>" data-size="normal"
			>
		</div>
		<?php else : ?>
		<div class="djeasycontact-g-recaptcha" id="djeasycontact-g-recaptcha-<?php echo $moduleId; ?>"
			data-sitekey="<?php echo $params->get('recaptcha_site_key', ''); ?>" data-size="invisible"
			data-callback="DJEasyContactSubmit<?php echo $module->id ?>"
			data-badge="<?php echo $invisible_captcha_badge_class; ?>">
		</div>
		<?php endif; ?>
	<?php endif; ?>

	<input type="hidden" name="dj-easy-contact-send-<?php echo $moduleId; ?>" value="true">
	<?php if ($style_file == '5') : ?>
	<div class="button-box">
		<button id="dj-easy-contact-send-<?php echo $moduleId; ?>"
			class="dj-simple-contact-form button submit <?php echo $mod_class_suffix; ?>">
			<span><?php echo JText::_('MOD_DJ_EASYCONTACT_BUTTON_LABEL'); ?></span>
		</button>
	</div>
	<?php else : ?>
	<div class="button-box">
		<input id="dj-easy-contact-send-<?php echo $moduleId; ?>"
			class="dj-simple-contact-form button submit <?php echo $mod_class_suffix; ?>" type="submit"
			value="<?php echo JText::_('MOD_DJ_EASYCONTACT_BUTTON_LABEL'); ?>" />
	</div>
	<?php endif ?>

</div>
