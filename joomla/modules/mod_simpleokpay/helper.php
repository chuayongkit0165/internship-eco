<?php
/**
 *
 * @package	    OKPay Module
 * @subpackage	OKPay Module
 * @version     1.0.0
 * @description OKPay Module
 * @copyright	  Copyright © 2013 - All rights reserved.
 * @license		  GNU General Public License v2.0
 * @author		  SoftPill.Eu
 * @author mail	mail@softpill.eu
 * @website		  www.softpill.eu
 *
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
class OKPayHelper {
		function generate_random_letters($length) {
      $random = '';
      for ($i = 0; $i < $length; $i++) {
        $random .= chr(rand(ord('a'), ord('z')));
      }
      return $random;
    }
    function _getWalletID ($method) {
  		echo '<pre style="font-size:12px;background:white;">';
  		print_r($method);
  		echo '</pre>';
  		return /*$method->sandbox ? $method->sandbox_wallet_id : */ $method->okpay_wallet_id;
  	}
  
  	function _getOkpayUrl ($method) {
  		$url = /*$method->sandbox ? 'www.sandbox.okpay.com' :*/ 'https://www.okpay.com';
  		return $url;
  	}
  
  	function _getOkpayUrlHttps ($method) {
  		$url = $this->_getOkpayUrl ($method);
  		$url = $url . '/process.html';
  		return $url;
  	}
  
  	/*
  	 * CheckOKPayIPs
  	 * Cannot be checked with Sandbox
  	 * From VM1.1
  	*/
  	function checkOkpayIps ($test_ipn) {
  		return;
  		// This function is temporary disabled
  		// Get the list of IP addresses for www.okpay.com and ipn.okpay.com
  		$okpay_iplist = array(
  								gethostbynamel ('www.okpay.com'),
  								gethostbynamel ('ipn.okpay.com'),
  								'127.0.0.1',
  								'127.1.1.1',
  								'8.8.8.8'
  								// or any another IP
  						);
  		$remote_hostname = gethostbyaddr ($_SERVER['REMOTE_ADDR']);
  		if (!in_array ($_SERVER['REMOTE_ADDR'], $okpay_iplist)) {
  			$mailsubject = "OKPAY IPN Transaction on your site: Possible fraud";
  			$mailbody = "Error code 506. Possible fraud. Error with REMOTE IP ADDRESS = " . $_SERVER['REMOTE_ADDR'] . ".
  						 The remote address of the script posting to this notify script does not match a valid OKPAY ip address\n
  						 These are the valid IP Addresses: $ips
  						 The Order ID received was: $invoice";
  			$this->sendEmailToVendorAndAdmins ($mailsubject, $mailbody);
  			exit();
  		}
  	}
	}

?>