<?php
ini_set("display_errors",0);
error_reporting(E_ALL);
/**
 *
 * @package	    OKPay Module
 * @subpackage	OKPay Module
 * @version     1.0.0
 * @description OKPay Module
 * @copyright	  Copyright © 2013 - All rights reserved.
 * @license		  GNU General Public License v2.0
 * @author		  SoftPill.Eu
 * @author mail	mail@softpill.eu
 * @website		  www.softpill.eu
 *
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if(isset($_REQUEST['okpay_action'])&&$_REQUEST['okpay_action']=='pay')
{
  if(isset($_REQUEST['working_module'])&&$_REQUEST['working_module']!=$module->id)
  {
    return;
  }
}

JHTML::_('behavior.modal');
$document=&JFactory::getDocument();
$document->addStyleSheet( juri::base().'modules/mod_simpleokpay/css/default.css' );

$params1  = getOKPayModParams($module->id);
$dchk=$params1['dchk'];
$lic=$params1['okpaylic'];
$the_domain=getOKPayDomain();
if($the_domain)
{
  $chk=md5($lic.$the_domain);
  if($dchk!=$chk){return false;}
}

?>


<div class="moduletable<?php echo $params->get('moduleclass_sfx'); ?>">
<?php

$vendor_missing=JText::_( 'SOP_MODULE_ERR_VENDOR' );
$success_url_missing=JText::_( 'SOP_MODULE_ERR_SUCCESS_URL' );
$failure_url_missing=JText::_( 'SOP_MODULE_ERR_FAILURE_URL' );
$payment_missing=JText::_( 'SOP_MODULE_ERR_PAYMENT' );
$product_name_missing=JText::_( 'SOP_MODULE_ERR_PRODUCT_NAME' );

$payment=$params->get('payment');
$payment_type=$params->get('payment_type');
$currency=$params->get('currency');
$payment_description=$params->get('payment_description');
$popup_description=$params->get('popup_description');
$button_text=$params->get('button_text');
$popup_button_text=$params->get('popup_button_text');
$success_url=$params->get('success_url');
$failure_url=$params->get('failure_url');
$vendor_name=trim($params->get('user'));


$default_country=($params->get('default_country')!="")?strtoupper($params->get('default_country')):"US";
$popup_width=($params->get('popup_width')+0)>0?$params->get('popup_width'):600;
$popup_height=($params->get('popup_height')+0)>0?$params->get('popup_height'):450;
$payment_type=$params->get('payment_type');
$amount_text=$params->get('amount_text');
$min_payment=$params->get('min_payment');
$product_name=$params->get('product_name');

if($vendor_name=='')
{
  //no vendor set error
  ?>
    <div class="okpay_error"><?php echo $vendor_missing;?></div>
  <?php
}
else if($success_url=='' || strlen(str_ireplace("http://","",$success_url))==0 || strlen(str_ireplace("https://","",$success_url))==0)
{
  //no success url set error
  ?>
    <div class="okpay_error"><?php echo $success_url_missing;?></div>
  <?php
}
else if($failure_url=='' || strlen(str_ireplace("http://","",$failure_url))==0 || strlen(str_ireplace("https://","",$failure_url))==0)
{
  //no success url set error
  ?>
    <div class="okpay_error"><?php echo $failure_url_missing;?></div>
  <?php
}
else if($product_name=='')
{
  ?>
    <div class="okpay_error"><?php echo $product_name_missing;?></div>
  <?php
}
else if(($payment=='' || ($payment+0)==0) && $payment_type==0)
{
  //no amount value set error
  ?>
    <div class="okpay_error"><?php echo $payment_missing;?></div>
  <?php
}
else
{
  //seams ok
  $action=isset($_REQUEST['okpay_action'])?$_REQUEST['okpay_action']:"";
  $country_codes_arr=array('AF'=>'Afghanistan', 'AL'=>'Albania', 'DZ'=>'Algeria', 'AS'=>'American Samoa', 'AD'=>'Andorra', 'AO'=>'Angola', 'AI'=>'Anguilla', 'AQ'=>'Antarctica', 'AG'=>'Antigua and Barbuda', 'AR'=>'Argentina', 'AM'=>'Armenia', 'AW'=>'Aruba', 'AU'=>'Australia', 'AT'=>'Austria', 'AZ'=>'Azerbaijan', 'BS'=>'Bahamas', 'BH'=>'Bahrain', 'BD'=>'Bangladesh', 'BB'=>'Barbados', 'BY'=>'Belarus', 'BE'=>'Belgium', 'BZ'=>'Belize', 'BJ'=>'Benin', 'BM'=>'Bermuda', 'BT'=>'Bhutan', 'BO'=>'Bolivia', 'BA'=>'Bosnia and Herzegowina', 'BW'=>'Botswana', 'BV'=>'Bouvet Island', 'BR'=>'Brazil', 'IO'=>'British Indian Ocean Territory', 'BN'=>'Brunei Darussalam', 'BG'=>'Bulgaria', 'BF'=>'Burkina Faso', 'BI'=>'Burundi', 'KH'=>'Cambodia', 'CM'=>'Cameroon', 'CA'=>'Canada', 'XC'=>'Canary Islands', 'CV'=>'Cape Verde', 'KY'=>'Cayman Islands', 'CF'=>'Central African Republic', 'TD'=>'Chad', 'CL'=>'Chile', 'CN'=>'China', 'CX'=>'Christmas Island', 'CC'=>'Cocos (Keeling) Islands', 'CO'=>'Colombia', 'KM'=>'Comoros', 'CG'=>'Congo', 'CK'=>'Cook Islands', 'CR'=>'Costa Rica', 'CI'=>'Cote D\'Ivoire', 'HR'=>'Croatia', 'CU'=>'Cuba', 'CY'=>'Cyprus', 'CZ'=>'Czech Republic', 'DK'=>'Denmark', 'DJ'=>'Djibouti', 'DM'=>'Dominica', 'DO'=>'Dominican Republic', 'TP'=>'East Timor', 'XE'=>'East Timor', 'EC'=>'Ecuador', 'EG'=>'Egypt', 'SV'=>'El Salvador', 'GQ'=>'Equatorial Guinea', 'ER'=>'Eritrea', 'EE'=>'Estonia', 'ET'=>'Ethiopia', 'FK'=>'Falkland Islands (Malvinas)', 'FO'=>'Faroe Islands', 'FJ'=>'Fiji', 'FI'=>'Finland', 'FR'=>'France', 'FX'=>'France, Metropolitan', 'GF'=>'French Guiana', 'PF'=>'French Polynesia', 'TF'=>'French Southern Territories', 'GA'=>'Gabon', 'GM'=>'Gambia', 'GE'=>'Georgia', 'DE'=>'Germany', 'GH'=>'Ghana', 'GI'=>'Gibraltar', 'GR'=>'Greece', 'GL'=>'Greenland', 'GD'=>'Grenada', 'GP'=>'Guadeloupe', 'GU'=>'Guam', 'GT'=>'Guatemala', 'GN'=>'Guinea', 'GW'=>'Guinea-bissau', 'GY'=>'Guyana', 'HT'=>'Haiti', 'HM'=>'Heard and Mc Donald Islands', 'HN'=>'Honduras', 'HK'=>'Hong Kong', 'HU'=>'Hungary', 'IS'=>'Iceland', 'IN'=>'India', 'ID'=>'Indonesia', 'IR'=>'Iran (Islamic Republic of)', 'IQ'=>'Iraq', 'IE'=>'Ireland', 'IL'=>'Israel', 'IT'=>'Italy', 'JM'=>'Jamaica', 'JP'=>'Japan', 'XJ'=>'Jersey', 'JO'=>'Jordan', 'KZ'=>'Kazakhstan', 'KE'=>'Kenya', 'KI'=>'Kiribati', 'KP'=>'Korea, Democratic People\'s Republic of', 'KR'=>'Korea, Republic of', 'KW'=>'Kuwait', 'KG'=>'Kyrgyzstan', 'LA'=>'Lao People\'s Democratic Republic', 'LV'=>'Latvia', 'LB'=>'Lebanon', 'LS'=>'Lesotho', 'LR'=>'Liberia', 'LY'=>'Libyan Arab Jamahiriya', 'LI'=>'Liechtenstein', 'LT'=>'Lithuania', 'LU'=>'Luxembourg', 'MO'=>'Macau', 'MK'=>'Macedonia, The Former Yugoslav Republic of', 'MG'=>'Madagascar', 'MW'=>'Malawi', 'MY'=>'Malaysia', 'MV'=>'Maldives', 'ML'=>'Mali', 'MT'=>'Malta', 'MH'=>'Marshall Islands', 'MQ'=>'Martinique', 'MR'=>'Mauritania', 'MU'=>'Mauritius', 'YT'=>'Mayotte', 'MX'=>'Mexico', 'FM'=>'Micronesia, Federated States of', 'MD'=>'Moldova, Republic of', 'MC'=>'Monaco', 'MN'=>'Mongolia', 'ME'=>'Montenegro', 'MS'=>'Montserrat', 'MA'=>'Morocco', 'MZ'=>'Mozambique', 'MM'=>'Myanmar', 'NA'=>'Namibia', 'NR'=>'Nauru', 'NP'=>'Nepal', 'NL'=>'Netherlands', 'AN'=>'Netherlands Antilles', 'NC'=>'New Caledonia', 'NZ'=>'New Zealand', 'NI'=>'Nicaragua', 'NE'=>'Niger', 'NG'=>'Nigeria', 'NU'=>'Niue', 'NF'=>'Norfolk Island', 'MP'=>'Northern Mariana Islands', 'NO'=>'Norway', 'OM'=>'Oman', 'PK'=>'Pakistan', 'PW'=>'Palau', 'PA'=>'Panama', 'PG'=>'Papua New Guinea', 'PY'=>'Paraguay', 'PE'=>'Peru', 'PH'=>'Philippines', 'PN'=>'Pitcairn', 'PL'=>'Poland', 'PT'=>'Portugal', 'PR'=>'Puerto Rico', 'QA'=>'Qatar', 'RE'=>'Reunion', 'RO'=>'Romania', 'RU'=>'Russian Federation', 'RW'=>'Rwanda', 'KN'=>'Saint Kitts and Nevis', 'LC'=>'Saint Lucia', 'VC'=>'Saint Vincent and the Grenadines', 'WS'=>'Samoa', 'SM'=>'San Marino', 'ST'=>'Sao Tome and Principe', 'SA'=>'Saudi Arabia', 'SN'=>'Senegal', 'RS'=>'Serbia', 'SC'=>'Seychelles', 'SL'=>'Sierra Leone', 'SG'=>'Singapore', 'SK'=>'Slovakia (Slovak Republic)', 'SI'=>'Slovenia', 'SB'=>'Solomon Islands', 'SO'=>'Somalia', 'ZA'=>'South Africa', 'GS'=>'South Georgia and the South Sandwich Islands', 'ES'=>'Spain', 'LK'=>'Sri Lanka', 'XB'=>'St. Barthelemy', 'XU'=>'St. Eustatius', 'SH'=>'St. Helena', 'PM'=>'St. Pierre and Miquelon', 'SD'=>'Sudan', 'SR'=>'Suriname', 'SJ'=>'Svalbard and Jan Mayen Islands', 'SZ'=>'Swaziland', 'SE'=>'Sweden', 'CH'=>'Switzerland', 'SY'=>'Syrian Arab Republic', 'TW'=>'Taiwan', 'TJ'=>'Tajikistan', 'TZ'=>'Tanzania, United Republic of', 'TH'=>'Thailand', 'DC'=>'The Democratic Republic of Congo', 'TG'=>'Togo', 'TK'=>'Tokelau', 'TO'=>'Tonga', 'TT'=>'Trinidad and Tobago', 'TN'=>'Tunisia', 'TR'=>'Turkey', 'TM'=>'Turkmenistan', 'TC'=>'Turks and Caicos Islands', 'TV'=>'Tuvalu', 'UG'=>'Uganda', 'UA'=>'Ukraine', 'AE'=>'United Arab Emirates', 'GB'=>'United Kingdom', 'US'=>'United States', 'UM'=>'United States Minor Outlying Islands', 'UY'=>'Uruguay', 'UZ'=>'Uzbekistan', 'VU'=>'Vanuatu', 'VA'=>'Vatican City State (Holy See)', 'VE'=>'Venezuela', 'VN'=>'Viet Nam', 'VG'=>'Virgin Islands (British)', 'VI'=>'Virgin Islands (U.S.)', 'WF'=>'Wallis and Futuna Islands', 'EH'=>'Western Sahara', 'YE'=>'Yemen', 'ZM'=>'Zambia', 'ZW'=>'Zimbabwe');
  if($action=='')
  {
  ?>
    <script type="text/javascript">
    var okpayform<?php echo $module->id;?>='';
    function openSqueeze<?php echo $module->id;?>(id)
    {
    	var options = {size: {x: <?php echo $popup_width;?>, y: <?php echo $popup_height;?>}, onClose: function(a){a.innerHTML='';} };
      if(SqueezeBox.options)
      {
        SqueezeBox.options=null;
      }
    	SqueezeBox.initialize(options);
      var content='';
      if(okpayform<?php echo $module->id;?>=='')
      {
        content=document.getElementById(id).innerHTML;
        okpayform<?php echo $module->id;?>=content;
        document.getElementById(id).innerHTML='';
      }
      else
      {
        content=okpayform<?php echo $module->id;?>;
      }
    	SqueezeBox.setContent('string',content);
      return false;
    }
    function validateokpayform<?php echo $module->id;?>()
    {
      var fname=document.getElementById('sspBillingFirstnames<?php echo $module->id;?>');
      var lname=document.getElementById('sspBillingSurname<?php echo $module->id;?>');
      var address=document.getElementById('sspBillingAddress1<?php echo $module->id;?>');
      var city=document.getElementById('sspBillingCity<?php echo $module->id;?>');
      var postcode=document.getElementById('sspBillingPostCode<?php echo $module->id;?>');
      var email=document.getElementById('sspCustomerEMail<?php echo $module->id;?>');
      var phone=document.getElementById('sspCustomerPhone<?php echo $module->id;?>');
      
      <?php
      if($payment_type==1)
      {
        ?>
      var amount=document.getElementById('payment_amount<?php echo $module->id;?>');
      var min_amount=parseFloat(<?php echo $min_payment;?>);
        <?php
      }
      ?>
      <?php
      if($payment_type==1)
      {
        ?>
        var the_amount=parseFloat(amount.value)+0;
        if(isNaN(the_amount))
        {
          alert('<?php echo JText::_( 'SOP_MODULE_ERR_NAN' );?>');
          amount.focus();
          return false;
        }
        else if(the_amount<min_amount)
        {
          alert('<?php echo JText::_( 'SOP_MODULE_ERR_LESS_AMNT' );?>');
          amount.focus();
          return false;
        }
        <?php
      }
      ?>
      if(fname.value=='')
      {
        alert('<?php echo JText::_( 'SOP_MODULE_ERR_FNAME' );?>');
        fname.focus();
        return false;
      }
      if(lname.value=='')
      {
        alert('<?php echo JText::_( 'SOP_MODULE_ERR_LNAME' );?>');
        lname.focus();
        return false;
      }
      if(address.value=='')
      {
        alert('<?php echo JText::_( 'SOP_MODULE_ERR_ADDR' );?>');
        address.focus();
        return false;
      }
      if(city.value=='')
      {
        alert('<?php echo JText::_( 'SOP_MODULE_ERR_CITY' );?>');
        city.focus();
        return false;
      }
      if(postcode.value=='')
      {
        alert('<?php echo JText::_( 'SOP_MODULE_ERR_POSTCODE' );?>');
        postcode.focus();
        return false;
      }
      if(email.value=='')
      {
        alert('<?php echo JText::_( 'SOP_MODULE_ERR_EMAIL' );?>');
        email.focus();
        return false;
      }
      if(!validateEmail(email.value))
      {
        alert('<?php echo JText::_( 'SOP_MODULE_ERR_EMAIL_VALID' );?>');
        email.focus();
        return false;
      }
      if(phone.value=='')
      {
        alert('<?php echo JText::_( 'SOP_MODULE_ERR_PHONE' );?>');
        phone.focus();
        return false;
      }
      return true;
    }
    function validateEmail(email) { 
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    } 
    </script>
    <div id="okpay_popup_content<?php echo $module->id;?>" class="okpay_popup_content" style="display:none;">
      <div class="okpay_popup_head">
        <?php echo $popup_description;?>
      </div>
      <div class="okpay_popup_content">
        <form action="" method="POST" name="OKPay<?php echo $module->id;?>" onSubmit="return validateokpayform<?php echo $module->id;?>();">
          <input type="hidden" name="working_module" value="<?=$module->id?>" />
          <input type="hidden" name="okpay_action" value="pay" />
          <table width="100%" class="okpay_popup_tbl">
          <tbody>
          <?php
          if($payment_type==1)
          {
            if($currency=='USD')
            {
              $currency_str='$';
            }
            else if($currency=='EUR')
            {
              $currency_str='&euro;';
            }
            else
            {
              $currency_str=$currency;
            }
            ?>
            <tr class="okpay_popup_row">
              <td>
                <label for="payment_amount<?php echo $module->id;?>" class="okpay_popup_lbl"><?php echo $amount_text;?></label>
              </td>
              <td>
                <input type="text" class="okpay_popup_payment_input" name="payment_amount" id="payment_amount<?php echo $module->id;?>" value="<?php echo $min_payment;?>" />
                <span class="okpay_popup_curr_sym"><?php echo $currency_str;?></span>
              </td>
            </tr>
            <?php
          }
          ?>
            <tr class="okpay_popup_row">
              <td>
                <label for="sspBillingFirstnames<?php echo $module->id;?>" class="okpay_popup_lbl"><?php echo JText::_( 'SOP_MODULE_BILLINGFIRSTNAMES' );?></label>
              </td>
              <td>
                <input type="text" class="okpay_popup_input" name="BillingFirstnames" id="sspBillingFirstnames<?php echo $module->id;?>" value="" />
              </td>
            </tr>
            <tr class="okpay_popup_row">
              <td>
                <label for="sspBillingSurname<?php echo $module->id;?>" class="okpay_popup_lbl"><?php echo JText::_( 'SOP_MODULE_BILLINGSURNAME' );?></label>
              </td>
              <td>
                <input type="text" class="okpay_popup_input" name="BillingSurname" id="sspBillingSurname<?php echo $module->id;?>" value="" />
              </td>
            </tr>
            <tr class="okpay_popup_row">
              <td>
                <label for="sspBillingAddress1<?php echo $module->id;?>" class="okpay_popup_lbl"><?php echo JText::_( 'SOP_MODULE_BILLINGADDRESS1' );?></label>
              </td>
              <td>
                <input type="text" class="okpay_popup_input" name="BillingAddress1" id="sspBillingAddress1<?php echo $module->id;?>" value="" />
              </td>
            </tr>
            <tr class="okpay_popup_row">
              <td>
                <label for="sspBillingCity<?php echo $module->id;?>" class="okpay_popup_lbl"><?php echo JText::_( 'SOP_MODULE_BILLINGCITY' );?></label>
              </td>
              <td>
                <input type="text" class="okpay_popup_input" name="BillingCity" id="sspBillingCity<?php echo $module->id;?>" value="" />
              </td>
            </tr>
            <tr class="okpay_popup_row">
              <td>
                <label for="sspBillingPostCode<?php echo $module->id;?>" class="okpay_popup_lbl"><?php echo JText::_( 'SOP_MODULE_BILLINGPOSTCODE' );?></label>
              </td>
              <td>
                <input type="text" class="okpay_popup_input" name="BillingPostCode" id="sspBillingPostCode<?php echo $module->id;?>" value="" />
              </td>
            </tr>
            <tr class="okpay_popup_row">
              <td>
                <label for="sspBillingCountry<?php echo $module->id;?>" class="okpay_popup_lbl"><?php echo JText::_( 'SOP_MODULE_BILLINGCOUNTRY' );?></label>
              </td>
              <td>
                <select id="sspBillingCountry<?php echo $module->id;?>" name="BillingCountry" class="okpay_popup_input_co">
                  <?php
                    foreach($country_codes_arr as $code=>$name)
                    {
                      $selected_str="";
                      if($code==$default_country)
                      {
                        $selected_str=' selected="selected"';
                      }
                      ?>
                      <option<?php echo $selected_str;?> value="<?php echo $code;?>"><?php echo $name;?></option>
                      <?php
                    }
                  ?>
                </select>
              </td>
            </tr>
            <tr class="okpay_popup_row">
              <td>
                <label for="sspCustomerEMail<?php echo $module->id;?>" class="okpay_popup_lbl"><?php echo JText::_( 'SOP_MODULE_EMAIL' );?></label>
              </td>
              <td>
                <input type="text" class="okpay_popup_input" name="CustomerEMail" id="sspCustomerEMail<?php echo $module->id;?>" value="" />
              </td>
            </tr>
            <tr class="okpay_popup_row">
              <td>
                <label for="sspCustomerPhone<?php echo $module->id;?>" class="okpay_popup_lbl"><?php echo JText::_( 'SOP_MODULE_PHONE' );?></label>
              </td>
              <td>
                <input type="text" class="okpay_popup_input" name="CustomerPhone" id="sspCustomerPhone<?php echo $module->id;?>" value="" />
              </td>
            </tr>
            <tr class="okpay_popup_submit_row">
              <td colspan="2">
                <input class="okpay_submit_pay" type="submit" value="<?php echo $popup_button_text;?>" />
              </td>
            </tr>
            </tbody>
          </table>
        </form>
      </div>
    </div>
    <div class="okpay_wrp">
      <div class="okpay_description">
        <?php echo $payment_description;?>
      </div>
      <input class="okpay_submit_pay" style="float:none;" type="button" value="<?php echo $button_text;?>" onClick="return openSqueeze<?php echo $module->id;?>('okpay_popup_content<?php echo $module->id;?>');" />
    </div>
    <?php
  }
  else if($action=='pay')
  {
    $error_check=0;
    $payment_amount=isset($_POST['payment_amount'])?$_POST['payment_amount']:0;
    $BillingFirstnames=isset($_POST['BillingFirstnames'])?$_POST['BillingFirstnames']:"";
    $BillingSurname=isset($_POST['BillingSurname'])?$_POST['BillingSurname']:"";
    $BillingAddress1=isset($_POST['BillingAddress1'])?$_POST['BillingAddress1']:"";
    $BillingCity=isset($_POST['BillingCity'])?$_POST['BillingCity']:"";
    $BillingPostCode=isset($_POST['BillingPostCode'])?$_POST['BillingPostCode']:"";
    $BillingCountry=isset($_POST['BillingCountry'])?$_POST['BillingCountry']:"";
    $CustomerEMail=isset($_POST['CustomerEMail'])?$_POST['CustomerEMail']:"";
    $CustomerPhone=isset($_POST['CustomerPhone'])?$_POST['CustomerPhone']:"";
    
    if($payment_type==1)
    {
      $payment=$payment_amount+0;
      if($payment==0 || $payment<$min_payment)
      {
        $error_check=1;
      }
    }
    if($payment==0)
    {
      $error_check=1;
    }
    if($BillingFirstnames=='')
    {
      $error_check=1;
    }
    if($BillingSurname=='')
    {
      $error_check=1;
    }
    if($BillingAddress1=='')
    {
      $error_check=1;
    }
    if($BillingCity=='')
    {
      $error_check=1;
    }
    if($BillingPostCode=='')
    {
      $error_check=1;
    }
    if($BillingCountry=='')
    {
      $error_check=1;
    }
    if($error_check)
    {
      //error here
      ?>
      <div class="okpay_submit_error"><?php echo JText::_( 'SOP_MODULE_SUBMIT_ERROR' );?></div>
      <?php
    }
    else
    {
      //all good submit the form
      
      $payment+=0;
      $payment=number_format($payment,2);
      
      $OKPayHelper = new OKPayHelper();
      $strVendorTxCode=uniqid('okpay_');
      $strVendorTxCode='okpay-'.$OKPayHelper->generate_random_letters(4);
      
      //add trans id in url
      if($success_url[strlen($success_url)-1]=='/')
      {
        $success_url.='?trans_id='.$strVendorTxCode;
      }
      else
      {
        $query = parse_url($success_url, PHP_URL_QUERY);
        if( $query ) {
            $success_url .= '&trans_id='.$strVendorTxCode;
        }
        else {
            $success_url .= '?trans_id='.$strVendorTxCode;
        }
      }
      if($failure_url[strlen($failure_url)-1]=='/')
      {
        $failure_url.='?trans_id='.$strVendorTxCode;
      }
      else
      {
        $query = parse_url($failure_url, PHP_URL_QUERY);
        if( $query ) {
            $failure_url .= '&trans_id='.$strVendorTxCode;
        }
        else {
            $failure_url .= '?trans_id='.$strVendorTxCode;
        }
      }
      
      $post_variables=array(
      "ok_invoice"=>$strVendorTxCode,
      "ok_receiver"=>$vendor_name,
      "ok_item_1_name"=>$product_name,
      "ok_item_1_quantity"=>1,
      "ok_item_1_price"=>$payment,
      "ok_currency"=>$currency,
      "ok_payer_first_name"=>$BillingFirstnames,
      "ok_payer_last_name"=>$BillingSurname,
      "ok_payer_email"=>$CustomerEMail,
      "ok_payer_country_code"=>$BillingCountry,
      "ok_payer_city"=>$BillingCity,
      "ok_payer_state"=>'',
      "ok_payer_street"=>$BillingAddress1,
      "ok_payer_zip"=>$BillingPostCode,
      "ok_return_success"=>$success_url,
      "ok_return_fail"=>$failure_url
      );
      
      $url = $OKPayHelper->_getOkpayUrlHttps ($method);
      
      $okpay_button='Click here to Pay';
      
      $html = '<html><head><title>Redirection</title></head><body><div style="margin: auto; text-align: center;">';
  		$html.= "\n".'<form name="OKPay" id="OKPay" action="'.$url.'" method="post">';
  	  foreach ($post_variables as $name => $value) {
  			$html .= '<input type="hidden" name="' . $name . '" value="' . htmlspecialchars ($value) . '" />';
  		}
  		$html.= '<input type="submit"  value="'.$okpay_button.'" />';
  		$html.= '</form></div>';
  		$html.= ' <script type="text/javascript">';
  		$html.= ' document.OKPay.submit();';
  		$html.= ' </script></body></html>';
      
      ob_end_clean();
      
      echo $html;exit;
    }
  }
}

?>
</div>
