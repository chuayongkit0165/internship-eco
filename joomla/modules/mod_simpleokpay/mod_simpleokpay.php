<?php
ini_set("display_errors",0);
error_reporting(E_ALL);
/**
 *
 * @package	    OKPay Module
 * @subpackage	OKPay Module
 * @version     1.0.0
 * @description OKPay Module
 * @copyright	  Copyright © 2013 - All rights reserved.
 * @license		  GNU General Public License v2.0
 * @author		  SoftPill.Eu
 * @author mail	mail@softpill.eu
 * @website		  www.softpill.eu
 *
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 if (!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}
if (!function_exists('getOKPayDomain'))
{
function getOKPayDomain()
{
  $domain=str_ireplace("www.","",$_SERVER['HTTP_HOST']);
  if(($domain+0)>0)
  {
    return false;
  }
  return $domain;
}
}
if (!function_exists('setOKPayModParams'))
{
function setOKPayModParams($param_array,$module_id)
{
  if ( count($param_array) > 0 )
  {
    $db = JFactory::getDbo();
    $db->setQuery('SELECT params FROM #__modules WHERE id = '.$module_id);
    $params1 = json_decode( $db->loadResult(), true );
    foreach ( $param_array as $name => $value )
    {
      $params1[ (string) $name ] = (string) $value;
    }
    $params1String = json_encode( $params1 );
    $db->setQuery('UPDATE #__modules SET params = ' .
    $db->quote( $params1String ) .
    ' WHERE id = '.$module_id);
    $db->query();
  }
}
}
if (!function_exists('getOKPayModParams'))
{
function getOKPayModParams($module_id)
{
  $db = JFactory::getDbo();
  $db->setQuery('SELECT params FROM #__modules WHERE id = '.$module_id);
  $params1 = json_decode( $db->loadResult(), true );
  return $params1;
}
}
if (!function_exists('OKPayModCheckLicense'))
{
function OKPayModCheckLicense($lic,$module)
{
  $the_domain=getOKPayDomain();
  if(!$the_domain)
  {
    return;
  }
  $checkurl='http://www.softpill.eu/check_complic.php?comp=simpleokpay&h='.$the_domain.'&lic='.$lic;
  if (!function_exists('curl_init'))
  {
    $output=@file_get_contents($checkurl);
    $the_domain=getOKPayDomain();
    if($output==$the_domain)
    {
      $ok = 1;
    }
  }
  else
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $checkurl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    $output = curl_exec($ch);
    curl_close($ch);
    $the_domain=getOKPayDomain();
    if($output==$the_domain)
    {
      $ok = 1;
    }
  }
  
  $application = JFactory::getApplication();
  if(@$ok==1)
  {
    $chk=md5($lic.$the_domain);
    setOKPayModParams(array('dchk'=>$chk,'okpaylic'=>$lic),$module->id);
    $application->enqueueMessage(JText::_( 'SOP_MODULE_LICENSE_OK' ), 'success');
  }
  else
  {
    setOKPayModParams(array('okpaylic'=>'','dchk'=>''),$module->id);
    $application->enqueueMessage(JText::_( 'SOP_MODULE_LICENSE_ERR_BAD' ), 'error');
  }
}
}

$params1  = getOKPayModParams($module->id);
$lic=@$params1['okpaylic'];
if($params->get('okpaylic')!=$lic)
{
  setOKPayModParams(array('okpaylic'=>$params->get('okpaylic')),$module->id);
  $lic=$params->get('okpaylic');
}
$application = JFactory::getApplication();
if($lic=="")
{
  $application->enqueueMessage(JText::_( 'SOP_MODULE_LICENSE_ERR' ), 'error');
  setOKPayModParams(array('okpaylic'=>'','dchk'=>''),$module->id);
}
else
{
  $dchk=@$params1['dchk'];
  if($dchk=='')
  {
    OKPayModCheckLicense($lic,$module);
  }
  else
  {
    $the_domain=getOKPayDomain();
    $chk=md5($lic.$the_domain);
    if($chk!=$dchk && $the_domain)//check
    {
      setOKPayModParams(array('okpaylic'=>'','dchk'=>''),$module->id);
      $application->enqueueMessage(JText::_( 'SOP_MODULE_LICENSE_ERR_BAD' ), 'error');
    }
  }
}

require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'helper.php');
require(JModuleHelper::getLayoutPath('mod_simpleokpay'));
?>